#pragma once

#include <string>
#include <iostream>

#include "logger.hpp"
#include "lexer/token.hpp"
#include "lexer/identifier.hpp"
#include "lexer/expression.hpp"
#include "lexer/symbol.hpp"
#include "lexer/location.hpp"
#include "lexer/section.hpp"

#include <pegtl.hh>

#include "muParserInt.h"


namespace nimble65
{
    //////////////////////////////////////////////////////////////////////
    // Data needed during a parsing run
    //////////////////////////////////////////////////////////////////////
    class ParsingData
    {
        public:
        // Preprocessor
        std::string curSrc;
        std::string preprocessedSource;
        std::string filename;
        int numPass = 0;
        // Information about the preprocessed source: At which pos a change
        // of source (file, macro etc.) begins, which size it has and at which
        // line of the source it starts.
        struct posInfo {
            int pos;
            int size;
            int line;
            std::string source;
        };
        std::vector<posInfo> posData;

        // Parser
        std::vector<std::unique_ptr<lexer::Location>> locationDefs;
        std::vector<std::unique_ptr<lexer::Token>> activeTokens;
        std::vector<std::unique_ptr<lexer::Symbol>> unresolvedSymbols;
        std::map<std::string, std::unique_ptr<lexer::Symbol>> symbols;
        std::string curVarstart = "0";
        mu::ParserInt exprParser;
        unsigned char fillByte = 0;

        // Insert a new posInfo item into the list. metaSize is the size of
        // the preprocessor statement that gets removed.
        void insertPosInfo(const std::string source, const int pos, const int size, const int metaSize, const int line);

        // Remove last active token and return unique_ptr to it
        std::unique_ptr<lexer::Token> popActiveToken();

        // Assumes last active token is an expression and pops and
        // returns the expr string
        std::string popExpression();
        // Same as above, but throws a parse_error if the expression
        // is enclosed in brackets (). Needs input from parsing run.
        std::string popNonBracketedExpression(const pegtl::input & in);

        // Assumes last active token is an identifier and pops and
        // returns the identifier string
        std::string popIdentifier();

        // Returns true if there is a current section
        bool sectionsExist() {
            return locationDefs.size() > 0 && locationDefs.back()->sectionDefs.size() > 0;
        }
        // Get the current section in the current location
        lexer::Section *getCurrentSection();

        // Removes active tokens that match the same location as the last
        // token, i.e. tokens that have previously been labeled mistakenly
        // (like the keyword "var" as Identifier or Expression).
        // Actually all tokens *after* the first one with the same location
        // get removed, as it can be multiple in case of longer expressions.
        // Returns true if at least one token was removed.
        bool removeActiveDuplicates();

        // Adds new symbol to the list of unresolved symbols. Doesn't care
        // for duplicates.
        void addNewSymbol(int line, int column, std::string ident, std::string expr);
        // Same as addNewSymbol, but checks for duplication and throws a compile_error
        // if the symbol already exists
        void addNewUniqueSymbol(int line, int column, std::string ident, std::string expr);

        // Returns true if a symbol with the same identifier already exsists in the
        // list of unresolved symbols
        bool symbolExists(std::string ident);
        // Deletes a symbol from the list of unresolved symbols. The symbol
        // to delete should exits.
        void deleteSymbol(std::string ident);
            
        // Adds a new section token to the end of the section defs of
        // the current location
        void addNewSection(lexer::Section *s);

        // Adds a new Instruction to the end of the instruction queue
        // of the current section in the current location
        void addNewInstruction(lexer::Instruction *i);

        // Attempts to resolve Symbols s. If successful, the value of
        // the expression is written to result and true is returned.
        bool tryToResolveSymbol(lexer::Symbol *s, int &result);

        // Resolves as many symbols as possible, using as many passes as
        // needed.
        void resolveSymbols();

        // Iteratively link all locations
        void linkLocations();

        // Generate binary out of all locations
        std::vector<unsigned char> generateBinary(int& startAddress);

        // Prints out the current list of active tokens at DEBUG level
        void printActiveTokens();
        // Prints out the current list of unresolved symbols at INFO level
        void printUnresolvedSymbols();
        // Prints out the current list of symbols at DEBUG level
        void printSymbols();

        private:
        // Return the smallest start address of all locations
        int getEarliestLocationStart();
        // Return the index of the location with the smallest start address
        int getEarliestLocationIndex();
    };

}
