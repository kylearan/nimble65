#include <string>
#include <regex>
#include <vector>
#include <cassert>

#include "logger.hpp"
#include "parsingdata.hpp"
#include "exceptions.hpp"
#include "lexer/token.hpp"
#include "lexer/expression.hpp"
#include "lexer/identifier.hpp"
#include "lexer/symbol.hpp"
#include "lexer/instrabsbase.hpp"
#include "muParserError.h"

namespace nimble65
{
//////////////////////////////////////////////////////////////////////
// Position info
//////////////////////////////////////////////////////////////////////

void ParsingData::insertPosInfo(
    const std::string source,
    const int pos,
    const int size,
    const int metaSize,
    const int line)
{
    bool inserted = false;
    for (size_t i = 0; i < posData.size(); ++i)
    {
        ParsingData::posInfo &pi = posData[i];
        if (pos >= pi.pos && pos < pi.pos + pi.size)
        {
            LOG(LogLevel::DEBUG) << "Pos: " << pos << ", size: " << size << ", metaSize: " << metaSize << ", source: " << source;
            LOG(LogLevel::DEBUG) << "Insert into pi.pos: " << pi.pos << ", pi.size: " << pi.size << ", pi.line: " << pi.line << ", pi.source: " << pi.source;
            // Split old entry and insert this one as new
            // xxx TODO: Why is line = 1 and not 0?
            ParsingData::posInfo newPi = {pos, size, line, source};
            ParsingData::posInfo afterPi = {
                pos + size,
                pi.size - metaSize - (pos - pi.pos),
                0, // TODO: xxx has to be current line in file?
                pi.source
            };
            pi.size = (pos - pi.pos);
            posData.insert(posData.begin() + i + 1, newPi);
            posData.insert(posData.begin() + i + 2, afterPi);
            // TODO: Check for zero size -> remove? pi? newPi?
            inserted = true;
            break;
        }
    }
    assert(inserted);

    LOG(LogLevel::DEBUG) << "New list:";
    for (auto &pi : posData)
    {
        LOG(LogLevel::DEBUG) << "Pos " << pi.pos << ", size " << pi.size << ", line " << pi.line << ", source " << pi.source;
    }
}

//////////////////////////////////////////////////////////////////////
// Token vector actions
//////////////////////////////////////////////////////////////////////

std::unique_ptr<lexer::Token> ParsingData::popActiveToken()
{
    std::unique_ptr<lexer::Token> t = std::move(activeTokens.back());
    activeTokens.pop_back();
    return t;
}

std::string ParsingData::popExpression()
{
    assert(activeTokens.size() >= 1);
    assert(activeTokens.back()->type == lexer::TokenType::expression);
    std::unique_ptr<lexer::Token> t = popActiveToken();
    auto e = static_cast<lexer::Expression *>(t.get());
    return e->expr;
}

std::string ParsingData::popNonBracketedExpression(const pegtl::input &in)
{
    auto e = popExpression();
    if (e[0] == '(')
    {
        throw pegtl::parse_error("Indirect addressing mode invalid for this instruction", in);
    }
    return e;
}

std::string ParsingData::popIdentifier()
{
    assert(activeTokens.size() >= 1);
    assert(activeTokens.back()->type == lexer::TokenType::identifier);
    std::unique_ptr<lexer::Token> t = popActiveToken();
    auto i = static_cast<lexer::Identifier *>(t.get());
    return i->identifier;
}

lexer::Section *ParsingData::getCurrentSection()
{
    assert(locationDefs.size() > 0);
    auto l = locationDefs.back().get();
    assert(l->sectionDefs.size() > 0);
    return l->sectionDefs.back().get();
}

bool ParsingData::removeActiveDuplicates()
{
    lexer::Token *lastToken = activeTokens.back().get();
    bool foundOne = false;
    for (size_t ti = 0; ti < activeTokens.size() - 1;)
    {
        lexer::Token *t = activeTokens[ti].get();
        if (t->isSameLocation(lastToken->line, lastToken->column))
        {
            activeTokens.erase(activeTokens.begin() + ti, activeTokens.end() - 1);
            foundOne = true;
        }
        else
        {
            ti++;
        }
    }
    if (foundOne)
    {
        LOG(LogLevel::DEBUG) << "...removed earlier token(s) at the same location";
    }
    return foundOne;
}

void ParsingData::addNewSymbol(int line, int column, std::string ident, std::string expr)
{
    auto s = new lexer::Symbol(line, column, ident, expr);
    unresolvedSymbols.push_back(std::unique_ptr<lexer::Symbol>(s));
}

void ParsingData::addNewUniqueSymbol(int line, int column, std::string ident, std::string expr)
{
    if (symbolExists(ident))
    {
        throw compile_error(line, column,
                            std::string("Redefinition of " + ident + "!"));
    }
    addNewSymbol(line, column, ident, expr);
}

bool ParsingData::symbolExists(std::string ident)
{
    for (auto const &s : unresolvedSymbols)
    {
        if (s->identifier == ident)
        {
            return true;
        }
    }
    return false;
}

void ParsingData::deleteSymbol(std::string ident)
{
    for (size_t i = 0; i < unresolvedSymbols.size(); ++i)
    {
        if (unresolvedSymbols[i]->identifier == ident)
        {
            unresolvedSymbols.erase(unresolvedSymbols.begin() + i);
            return;
        }
    }
    assert(false);
}

void ParsingData::addNewSection(lexer::Section *s)
{
    auto l = locationDefs.back().get();
    l->sectionDefs.push_back(std::unique_ptr<lexer::Section>(s));
    // Create symbol so that redefinitions can be spotted.
    // Symbol for section "x" will resolve to "__x_address"
    std::string secSymbol = std::string("__" + s->label + "_address");
    addNewUniqueSymbol(s->line, s->column, s->label, secSymbol);
}

void ParsingData::addNewInstruction(lexer::Instruction *i)
{
    auto s = getCurrentSection();
    s->instructions.push_back(std::unique_ptr<lexer::Instruction>(i));
    // Increase section size accordingly. If the size of the instruction
    // is not yet known, append the respective size expression to the
    // current unresolved size string for later label resolving, and
    // also add the address expression separately to a list of
    // unresolved address size expressions for sections size estimation
    // during linking.
    s->size += i->size();
    if (!i->hasFixedSize)
    {
        s->unresolvedSize.append("+" + i->sizeExpr());
    }
}

//////////////////////////////////////////////////////////////////////
// Resolve symbols
//////////////////////////////////////////////////////////////////////

bool ParsingData::tryToResolveSymbol(lexer::Symbol *s, int &result)
{
    exprParser.SetExpr(s->expression);
    try
    {
        result = (int)exprParser.Eval();
    }
    catch (mu::ParserInt::exception_type &e)
    {
        if (e.GetCode() == mu::EErrorCodes::ecINVALID_SIZE ||
            e.GetCode() == mu::EErrorCodes::ecINVALID_LOHISIZE)
        {
            throw compile_error(s->line, s->column,
                                std::string("Expression in '" + s->expression + "' is not an unsigned 16 bit value!"));
        }
        return false;
    }
    return true;
}

void ParsingData::resolveSymbols()
{
    int resolved;
    do
    {
        resolved = 0;
        for (size_t si = 0; si < unresolvedSymbols.size();)
        {
            lexer::Symbol *s = unresolvedSymbols[si].get();
            int result;
            if (tryToResolveSymbol(s, result))
            {
                // Check for redefinition of the same identifier
                if (symbols.count(s->identifier) != 0)
                {
                    auto offender = symbols[s->identifier].get();
                    throw compile_error(offender->line, offender->column,
                                        std::string("Redefinition of " + s->identifier + "!"));
                    si++;
                }
                else
                {
                    // Add value to Symbol and muParser
                    s->value = double(result);
                    exprParser.DefineVar(s->identifier, &(s->value));
                    // Move s from list of unresolved symbols to symbols
                    symbols[s->identifier] = std::move(unresolvedSymbols[si]);
                    ;
                    unresolvedSymbols.erase(unresolvedSymbols.begin() + si);
                    resolved++;
                }
            }
            else
            {
                si++;
            }
        }
        LOG(LogLevel::DEBUG) << "Pass: " << resolved << " symbols resolved.";
    } while (resolved > 0);
}

//////////////////////////////////////////////////////////////////////
// Linking
//////////////////////////////////////////////////////////////////////

void ParsingData::linkLocations()
{
    for (auto &&l : locationDefs)
    {
        LOG(LogLevel::VERBOSE) << "Linking location " << l->number << "...";
        l->initMemory(this);
        l->linkSections(this);
        LOG(LogLevel::VERBOSE) << "Linking location " << l->number << " done.";
    }
}

int ParsingData::getEarliestLocationStart()
{
    int min = locationDefs[0]->getStartAddress();
    for (auto const &l : locationDefs)
    {
        if (l->getStartAddress() < min)
        {
            min = l->getStartAddress();
        }
    }
    return min;
}

int ParsingData::getEarliestLocationIndex()
{
    int min = locationDefs[0]->getStartAddress();
    int result = 0;
    for (size_t i = 0; i < locationDefs.size(); ++i)
    {
        if (locationDefs[i]->getStartAddress() < min)
        {
            min = locationDefs[i]->getStartAddress();
            result = (int)i;
        }
    }
    return result;
}

std::vector<unsigned char> ParsingData::generateBinary(int &startAddress)
{
    std::vector<unsigned char> binary;
    int curAddress = getEarliestLocationStart();
    startAddress = curAddress;
    while (locationDefs.size() > 0)
    {
        // Find next location
        int locIndex = getEarliestLocationIndex();
        lexer::Location *loc = locationDefs[locIndex].get();
        LOG(LogLevel::DEBUG) << "Processing location " << loc->number << " with start address $" << std::hex << loc->getStartAddress() << std::dec;
        if (loc->getStartAddress() < curAddress)
        {
            throw link_error(loc->line, loc->column,
                             std::string("Location " + std::to_string(loc->number) + " overlaps with a previous location!"));
        }
        // If there is a gap between last and this location, fill it
        if (loc->getStartAddress() > curAddress)
        {
            binary.insert(binary.end(), loc->getStartAddress() - curAddress, fillByte);
            curAddress = loc->getStartAddress();
        }
        // Add location to binary
        auto locBinary = loc->generateBinary(exprParser, fillByte);
        binary.insert(binary.end(), locBinary.begin(), locBinary.end());
        curAddress += locBinary.size();
        // Location successfully processed: Remove it
        locationDefs.erase(locationDefs.begin() + locIndex);
    }
    return binary;
}

//////////////////////////////////////////////////////////////////////
// Output
//////////////////////////////////////////////////////////////////////

void ParsingData::printActiveTokens()
{
    LOG(LogLevel::DEBUG) << " --------------------";
    LOG(LogLevel::DEBUG) << "| Current list of active tokens:";
    for (auto &&t : activeTokens)
    {
        switch (t.get()->type)
        {
        case lexer::TokenType::token:
            LOG(LogLevel::DEBUG) << "| " << t.get()->toString();
            break;
        case lexer::TokenType::identifier:
            LOG(LogLevel::DEBUG) << "| " << static_cast<lexer::Identifier *>(t.get())->toString();
            break;
        case lexer::TokenType::expression:
            LOG(LogLevel::DEBUG) << "| " << static_cast<lexer::Expression *>(t.get())->toString();
            break;
        case lexer::TokenType::symbol:
            LOG(LogLevel::DEBUG) << "| " << static_cast<lexer::Symbol *>(t.get())->toString();
            break;
        default:
            LOG(LogLevel::DEBUG) << "| "
                                 << "UNKNOWN";
        }
    }
    LOG(LogLevel::DEBUG) << " --------------------\n";
}

void ParsingData::printUnresolvedSymbols()
{
    LOG(LogLevel::INFO) << "Unresolved symbols:";
    for (std::vector<std::unique_ptr<lexer::Symbol>>::const_iterator i = unresolvedSymbols.begin(); i != unresolvedSymbols.end(); ++i)
    {
        LOG(LogLevel::INFO) << "    " << i->get()->identifier << " = " << i->get()->expression << " at " << i->get()->line << ", " << i->get()->column;
    }
}

void ParsingData::printSymbols()
{
    LOG(LogLevel::DEBUG) << "Known symbols:";
    for (std::map<std::string, std::unique_ptr<lexer::Symbol>>::const_iterator i = symbols.begin(); i != symbols.end(); ++i)
    {
        LOG(LogLevel::DEBUG) << "    " << i->first << " = $" << link_error::toHex(int(i->second->value));
    }
}

} // namespace nimble65
