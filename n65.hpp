#pragma once

#include <string>
#include "nimble65.hpp"
#include "parsingdata.hpp"

namespace nimble65 {

class N65 {
public:
    N65(NimbleConfig & conf) : conf(conf) {};
    bool preprocessSource(ParsingData & data);
    bool parseSource(ParsingData & data);
    bool resolveSymbolsAndLink(ParsingData & data);
    bool generateAndWriteBinary(ParsingData & data);
                
private:
    NimbleConfig conf;
};

}

