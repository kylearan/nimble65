#pragma once

#include <string>
#include "parsingdata.hpp"

namespace nimble65 {

struct NimbleConfig {
    std::string inFilename;
    std::string outFilename;
    bool writeSymFile = false;
    std::string symFilename;
    unsigned char fillByte = 0;
    int maxPasses = 100;
};

}

