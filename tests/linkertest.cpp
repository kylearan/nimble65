#include "catch.hpp"

#include <string>
#include <iostream>
#include <vector>
#include <typeinfo>

#include <pegtl.hh>
#include <pegtl/analyze.hh>
#include <pegtl/trace.hh>

#include "logger.hpp"
#include "exceptions.hpp"
#include "parser/assembler/common.hpp"
#include "parser/assembler/grammar.hpp"
#include "lexer/location.hpp"
#include "lexer/section.hpp"
#include "lexer/instrdata.hpp"
#include "lexer/instrimp.hpp"
#include "lexer/instrimm.hpp"
#include "lexer/instrabs.hpp"
#include "lexer/instrabsx.hpp"
#include "lexer/instrabsy.hpp"
#include "lexer/instrrel.hpp"
#include "lexer/instrind.hpp"
#include "lexer/instrindx.hpp"
#include "lexer/instrindy.hpp"

#include "parsingdata.hpp"

#include "muParserInt.h"
#include "muParserBase.h"
#include "muParserError.h"

#include "lexer/symbol.hpp"

extern LogConfig logCfg;

namespace nimble65 { namespace tests {

void prepareForBinary(std::string src, std::string desc, ParsingData& data) {
    pegtl::parse<nimble65::parser::assembler::grammar, nimble65::parser::assembler::action, nimble65::parser::assembler::errors>(
        src, desc, data);
    data.resolveSymbols();
    data.linkLocations();
    data.resolveSymbols();
}

std::string secStr(int size) {
    std::string s("section s" + std::to_string(size) + " { dc.b 0");
    for (int i = 1; i < size; ++i) {
        s.append(", " + std::to_string(i));
    }
    s.append(" }");
    return s;
}
    
TEST_CASE("Linking from_to location works", "[LINK_LOC_FROM_TO]") {
    ParsingData data;
    auto l = new lexer::Location(1, 1, "$1000", "$1fff");
    data.locationDefs.push_back(std::unique_ptr<lexer::Location>(l));

    SECTION("org section before start") {
        data.addNewSection(new lexer::Section(2, 2, "s", "$fff"));
        data.addNewInstruction(new lexer::InstrData(0, 0, 0, "dc.w", "9999", 2));
        l->initMemory(&data);
        REQUIRE_THROWS(l->linkOrgSections(&data));
    }
    SECTION("org section at start") {
        data.addNewSection(new lexer::Section(2, 2, "s", "$1000"));
        data.addNewInstruction(new lexer::InstrData(0, 0, 0, "dc.w", "9999", 2));
        l->initMemory(&data);
        REQUIRE_NOTHROW(l->linkOrgSections(&data));
        REQUIRE(l->sectionDefs.size() == 0);
        REQUIRE(l->linkedSections.size() == 1);
        REQUIRE(l->linkedSections.front()->getStartAddress() == 0x1000);
        REQUIRE(l->freeMem.size() == 1);
        REQUIRE(l->freeMem[0].start == 0x1002);
        REQUIRE(l->freeMem[0].size == 0x1000 - 2);
    }
    SECTION("org section in the middle") {
        data.addNewSection(new lexer::Section(2, 2, "s", "$1002"));
        data.addNewInstruction(new lexer::InstrData(0, 0, 0, "dc.w", "9999", 2));
        l->initMemory(&data);
        REQUIRE_NOTHROW(l->linkOrgSections(&data));
        REQUIRE(l->sectionDefs.size() == 0);
        REQUIRE(l->linkedSections.size() == 1);
        REQUIRE(l->linkedSections.front()->getStartAddress() == 0x1002);
        REQUIRE(l->freeMem.size() == 2);
        REQUIRE(l->freeMem[0].start == 0x1000);
        REQUIRE(l->freeMem[0].size == 2);
        REQUIRE(l->freeMem[1].start == 0x1004);
        REQUIRE(l->freeMem[1].size == 0x1000 - 4);
    }
    SECTION("org section at the end") {
        data.addNewSection(new lexer::Section(2, 2, "s", "$1ffe"));
        data.addNewInstruction(new lexer::InstrData(0, 0, 0, "dc.w", "9999", 2));
        l->initMemory(&data);
        REQUIRE_NOTHROW(l->linkOrgSections(&data));
        REQUIRE(l->sectionDefs.size() == 0);
        REQUIRE(l->linkedSections.size() == 1);
        REQUIRE(l->linkedSections.front()->getStartAddress() == 0x1ffe);
        REQUIRE(l->freeMem.size() == 1);
        REQUIRE(l->freeMem[0].start == 0x1000);
        REQUIRE(l->freeMem[0].size == 0x1000 - 2);
    }
    SECTION("org section overlapping the end") {
        data.addNewSection(new lexer::Section(2, 2, "s", "$1fff"));
        data.addNewInstruction(new lexer::InstrData(0, 0, 0, "dc.w", "9999", 2));
        l->initMemory(&data);
        REQUIRE_THROWS(l->linkOrgSections(&data));
    }
    SECTION("org section after end") {
        data.addNewSection(new lexer::Section(2, 2, "s", "$2000"));
        data.addNewInstruction(new lexer::InstrData(0, 0, 0, "dc.w", "9999", 2));
        l->initMemory(&data);
        REQUIRE_THROWS(l->linkOrgSections(&data));
    }
    SECTION("two non-overlapping org sections") {
        data.addNewSection(new lexer::Section(1, 1, "s1", "$1100"));
        data.addNewInstruction(new lexer::InstrData(0, 0, 0, "dc.w", "9999", 2));
        data.addNewSection(new lexer::Section(2, 2, "s2", "$1200"));
        data.addNewInstruction(new lexer::InstrData(0, 0, 0, "dc.w", "9999", 2));
        l->initMemory(&data);
        REQUIRE_NOTHROW(l->linkOrgSections(&data));
        REQUIRE(l->sectionDefs.size() == 0);
        REQUIRE(l->linkedSections.size() == 2);
        REQUIRE(l->freeMem.size() == 3);
        REQUIRE(l->freeMem[0].start == 0x1000);
        REQUIRE(l->freeMem[0].size == 0x100);
        REQUIRE(l->freeMem[1].start == 0x1102);
        REQUIRE(l->freeMem[1].size == 0x100 - 2);
        REQUIRE(l->freeMem[2].start == 0x1202);
        REQUIRE(l->freeMem[2].size == 0x2000 - 0x1202);
    }
    SECTION("two overlapping org sections") {
        data.addNewSection(new lexer::Section(1, 1, "s1", "$1002"));
        data.addNewInstruction(new lexer::InstrData(0, 0, 0, "dc.w", "9999", 2));
        data.addNewSection(new lexer::Section(2, 2, "s2", "$1001"));
        data.addNewInstruction(new lexer::InstrData(0, 0, 0, "dc.w", "9999", 2));
        l->initMemory(&data);
        REQUIRE_THROWS(l->linkOrgSections(&data));
    }

}

TEST_CASE("Address symbol for section after linking works", "[LINK_RESOLVE]") {
    ParsingData data;
    pegtl::parse<nimble65::parser::assembler::grammar, nimble65::parser::assembler::action, nimble65::parser::assembler::errors>("location $1000 { section l org $1000 {} }", "Link resolve", data);
    data.resolveSymbols();
    REQUIRE(data.unresolvedSymbols.size() == 1);
    data.linkLocations();
    data.resolveSymbols();
    REQUIRE(data.unresolvedSymbols.size() == 0);
}


TEST_CASE("Linking sections without constraints work", "[LINK_SEC_NO_CONSTRAINTS]") {
    ParsingData data;

    SECTION("5, 3, 1") {
        prepareForBinary("location $1000 to $1100 { " + secStr(5) + secStr(3) + secStr(1) + "}", "5, 3, 1", data);
        REQUIRE(data.locationDefs[0]->linkedSections[0]->getStartAddress() == 0x1000);
        REQUIRE(data.locationDefs[0]->linkedSections[1]->getStartAddress() == 0x1005);
        REQUIRE(data.locationDefs[0]->linkedSections[2]->getStartAddress() == 0x1008);                
    }
    SECTION("1, 3, 5") {
        prepareForBinary("location $1000 to $1100 { " + secStr(1) + secStr(3) + secStr(5) + "}", "1, 3, 5", data);
        REQUIRE(data.locationDefs[0]->linkedSections[0]->getStartAddress() == 0x1000);
        REQUIRE(data.locationDefs[0]->linkedSections[1]->getStartAddress() == 0x1005);
        REQUIRE(data.locationDefs[0]->linkedSections[2]->getStartAddress() == 0x1008);                
    }
    SECTION("Second section too big") {
        REQUIRE_THROWS(prepareForBinary("location $1000 to $1005 { " + secStr(2) + secStr(5) + "}", "second section too big", data));
    }
    SECTION("Empty section") {
        prepareForBinary("location $1000 to $1100 { section empty {} " + secStr(1) + secStr(3) + secStr(5) + "}", "Empty section", data);
        REQUIRE(data.locationDefs[0]->linkedSections[0]->getStartAddress() == 0x1000);
        REQUIRE(data.locationDefs[0]->linkedSections[1]->getStartAddress() == 0x1005);
        REQUIRE(data.locationDefs[0]->linkedSections[2]->getStartAddress() == 0x1008);                
    }
    SECTION("Fixed 2nd half + non-fixed") {
        prepareForBinary("location $1000 to $1100 { section f org $1090 { nop } " + secStr(5) + "}", "fixed 2nd half + non-fixed", data);
        REQUIRE(data.locationDefs[0]->linkedSections[0]->getStartAddress() == 0x1090);
        REQUIRE(data.locationDefs[0]->linkedSections[1]->getStartAddress() == 0x1091);
    }
    SECTION("Fixed 1st half + non-fixed") {
        prepareForBinary("location $1000 to $1100 { section f org $1050 { nop } " + secStr(5) + "}", "fixed 1st half + non-fixed", data);
        REQUIRE(data.locationDefs[0]->linkedSections[0]->getStartAddress() == 0x1050);
        REQUIRE(data.locationDefs[0]->linkedSections[1]->getStartAddress() == 0x1000);
    }
}


TEST_CASE("Binary output size is correct", "[LINK_BINARY_SIZE]") {
    ParsingData data;
    int start = 0;

    SECTION("One from_to location, no section") {
        prepareForBinary("location $1000 to $1fff {}", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 0x1000);
    }
    SECTION("One from_to location, one section at start") {
        prepareForBinary("location $1000 to $1fff { section l org $1000 { nop } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 0x1000);
    }
    SECTION("One from_to location, one section in middle") {
        prepareForBinary("location $1000 to $1fff { section l org $1800 { nop } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 0x1000);
    }
    SECTION("One from_to location, two sections in the middle") {
        prepareForBinary("location $1000 to $1fff { section l1 org $1200 { nop } section l2 org $1800 { nop } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 0x1000);
    }
    SECTION("Two consecutive from_to locations") {
        prepareForBinary("location $1000 to $1fff { section l1 org $1800 { nop } } location $2000 to $2fff { section l2 org $2800 { nop } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 0x2000);
    }
    SECTION("Two non-consecutive from_to locations") {
        prepareForBinary("location $1000 to $1fff { section l1 org $1800 { nop } } location $3000 to $3fff {}", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 0x3000);
    }
    SECTION("Two overlapping locations") {
        prepareForBinary("location $1000 to $1fff { section l1 org $1800 { nop } } location $1800 to $1fff { section l2 org $1900 { nop } }", "binary size", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }

    SECTION("One from location, no section") {
        prepareForBinary("location $1000 {}", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 0);
    }
    SECTION("One from location, one section at start") {
        prepareForBinary("location $1000 { section l org $1000 { nop } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 1);
    }
    SECTION("One from location, one section at later address") {
        prepareForBinary("location $1000 { section l org $1005 { nop } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 6);
    }
    SECTION("One from location, one non-org section ") {
        prepareForBinary("location $1000 { section l { dc.w 42 } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 2);
    }
    SECTION("One from location, one org and one non-org section ") {
        prepareForBinary("location $1000 { section l1 org $1010 { dc.w 42 } section l2 { dc.w 1 } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 0x12);
    }
    SECTION("One from location, one org and one bigger non-org section ") {
        prepareForBinary("location $1000 { section l1 org $1002 { dc.w 42 } section l2 { dc.w 1, 2 } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 8);
    }
    SECTION("One from location, one org and one exactly fitting non-org section ") {
        prepareForBinary("location $1000 { section l1 org $1002 { dc.w 42 } section l2 { dc.w 2 } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 4);
    }

    SECTION("One from location, one align section ") {
        prepareForBinary("location $1000 { section l1 align $100 { dc.w 42 } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 2);
    }
    SECTION("One non-aligned from location, one align section ") {
        prepareForBinary("location $0ff0 { section l1 align $100 { dc.w 42 } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x0ff0);
        REQUIRE(bin.size() == 0x12);
    }
    SECTION("One from_to location, one non-fitting align section ") {
        REQUIRE_THROWS(prepareForBinary("location $1001 to $10ff { section l1 align $100 { dc.b 42 } }", "binary size", data));
    }
    
    SECTION("One from location, one align offset section ") {
        prepareForBinary("location $1000 { section l1 align $100 offset 2 { dc.w 42 } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 4);
    }
    SECTION("One from location, one non-org and one fitting align offset section ") {
        prepareForBinary("location $1000 { section l1 { dc.w 1, 2 } section l2 align $100 offset 4 { dc.w 42 } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 6);
    }
    SECTION("One from location, one non-org and one align offset section with gap") {
        prepareForBinary("location $1000 { section l1 { dc.w 1, 2 } section l2 align $100 offset 6 { dc.w 42 } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 8);
    }
    SECTION("One from_to location, one non-org and one fitting align offset section") {
        prepareForBinary("location $1000 to $1007 { section l1 { dc.w 1 } section l2 align $100 offset 2 { dc.w 2, 3 } }", "binary size", data);
        auto bin = data.generateBinary(start);
        REQUIRE(start == 0x1000);
        REQUIRE(bin.size() == 8);
    }
}


TEST_CASE("Constraints work", "[LINK_CONSTRAINTS]") {
    ParsingData data;
    int start = 0;

    SECTION("samepage") {
        prepareForBinary("location $10ff { section l { samepage{ dc.w 1, 2 } } }", "samepage", data);
        REQUIRE(data.locationDefs[0]->linkedSections[0]->getStartAddress() == 0x1100);
    }
    SECTION("samepage impossible") {
        REQUIRE_THROWS(prepareForBinary("location $10fe to $1102 { section l { samepage{ dc.w 1, 2 } } }", "samepage", data));
    }
    SECTION("samepage+offset impossible") {
        REQUIRE_THROWS(prepareForBinary("location $1000 { section l align $100 offset $fe { samepage{ dc.w 1, 2 } } }", "samepage", data));
    }

    SECTION("crosspage") {
        prepareForBinary("location $1000 { section l { crosspage{ dc.w 1 } } }", "crosspage", data);
        REQUIRE(data.locationDefs[0]->linkedSections[0]->getStartAddress() == 0x10ff);
    }
    SECTION("crosspage impossible") {
        REQUIRE_THROWS(prepareForBinary("location $1000 to $10ff { section l { crosspage{ dc.w 1 } } }", "crosspage", data));
    }
    SECTION("crosspage+align impossible") {
        REQUIRE_THROWS(prepareForBinary("location $1000 { section l align $100 { crosspage{ dc.w 1 } } }", "samepage", data));
    }
    SECTION("crosspage+offset impossible") {
        REQUIRE_THROWS(prepareForBinary("location $1000 { section l align $100 offset 4 { crosspage{ dc.w 1 } } }", "samepage", data));
    }

}

}}
