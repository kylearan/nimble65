#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <string>
#include <iostream>
#include <vector>
#include <typeinfo>

#include <pegtl.hh>
#include <pegtl/analyze.hh>
#include <pegtl/trace.hh>

#include "logger.hpp"

#include "logger.hpp"
#include "exceptions.hpp"
#include "parser/assembler/common.hpp"
#include "parser/assembler/grammar.hpp"
#include "lexer/location.hpp"
#include "lexer/section.hpp"
#include "lexer/instrdata.hpp"
#include "lexer/instrimp.hpp"
#include "lexer/instrimm.hpp"
#include "lexer/instrabs.hpp"
#include "lexer/instrabsx.hpp"
#include "lexer/instrabsy.hpp"
#include "lexer/instrrel.hpp"
#include "lexer/instrind.hpp"
#include "lexer/instrindx.hpp"
#include "lexer/instrindy.hpp"

#include "parsingdata.hpp"

#include "muParserInt.h"
#include "muParserBase.h"
#include "muParserError.h"

#include "lexer/symbol.hpp"

LogConfig logCfg{false, LogLevel::SILENT};

namespace nimble65 { namespace tests {

// Test a PEGTL parsing run with S a string to be parsed and T the test name
#define TEST_PARSING(S, T) \
    { \
        bool result = pegtl::parse<nimble65::parser::assembler::grammar, nimble65::parser::assembler::action, nimble65::parser::assembler::errors>(S, T, data); \
        REQUIRE(result); \
    }

// Test if a PEGTL parsing run throws an exception
#define TEST_PARSING_EXCEPTION(S, T) \
    { \
        bool result = true; \
        try { \
                pegtl::parse<nimble65::parser::assembler::grammar, nimble65::parser::assembler::action, nimble65::parser::assembler::errors>(S, T, data); \
            } catch (...) { result = false; } \
        REQUIRE(!result); \
    }

#define TEST_PARSING_NOTHROW(S, T) \
{ \
    bool result = true; \
    try { \
            pegtl::parse<nimble65::parser::assembler::grammar, nimble65::parser::assembler::action, nimble65::parser::assembler::errors>(S, T, data); \
        } catch (...) { result = false; } \
    REQUIRE(result); \
}

enum InstrType : int {I_ABS, I_ABSX, I_ABSY, I_DATA, I_IMM, I_IMP, I_IND, I_INDX, I_INDY, I_REL};

// Creates a vector with InstrType int values out of the current section in data
std::vector<int> sectionToTestVector(ParsingData *data) {
    lexer::Section *s = data->getCurrentSection();
    std::vector<int> instrTypes;
    for (size_t i = 0; i < s->instructions.size(); ++i) {
        std::unique_ptr<lexer::Instruction> ins = std::move(s->instructions[i]);
        if (dynamic_cast<lexer::InstrAbs *>(ins.get()) != nullptr) {
            instrTypes.push_back(I_ABS);
        } else if  (dynamic_cast<lexer::InstrAbsX *>(ins.get()) != nullptr) {
            instrTypes.push_back(I_ABSX);
        } else if  (dynamic_cast<lexer::InstrAbsY *>(ins.get()) != nullptr) {
            instrTypes.push_back(I_ABSY);
        } else if  (dynamic_cast<lexer::InstrData *>(ins.get()) != nullptr) {
            instrTypes.push_back(I_DATA);
        } else if  (dynamic_cast<lexer::InstrImm *>(ins.get()) != nullptr) {
            instrTypes.push_back(I_IMM);
        } else if  (dynamic_cast<lexer::InstrImp *>(ins.get()) != nullptr) {
            instrTypes.push_back(I_IMP);
        } else if  (dynamic_cast<lexer::InstrInd *>(ins.get()) != nullptr) {
            instrTypes.push_back(I_IND);
        } else if  (dynamic_cast<lexer::InstrIndX *>(ins.get()) != nullptr) {
            instrTypes.push_back(I_INDX);
        } else if  (dynamic_cast<lexer::InstrIndY *>(ins.get()) != nullptr) {
            instrTypes.push_back(I_INDY);
        } else if  (dynamic_cast<lexer::InstrRel *>(ins.get()) != nullptr) {
            instrTypes.push_back(I_REL);
        } else {
            throw std::runtime_error("Unknown instruction type found");
        }
    }

    return instrTypes;
}


////////////////////////////////////////////////////////////////////////////////
// PEGTL grammar validation
////////////////////////////////////////////////////////////////////////////////

TEST_CASE( "PEGTL grammar is valid", "[PEGTL]" ) {

    REQUIRE(pegtl::analyze< parser::assembler::grammar >() == 0);

}


////////////////////////////////////////////////////////////////////////////////
// PEGTL parsing
////////////////////////////////////////////////////////////////////////////////

TEST_CASE( "Parsing with PEGTL works", "[PARSING]" ) {
    ParsingData data;

    SECTION("location from") {
        TEST_PARSING("location 1 {}", "location from")
    }
    SECTION("location from to") {
        TEST_PARSING("location 1 to 2 {}", "location from to")
    }

    SECTION("section label") {
        TEST_PARSING("location 1 { section l {} }", "section label")
    }
    SECTION("section label align") {
        TEST_PARSING("location 1 { section l align 1 {} }", "section label align")
    }
    SECTION("section label align offset") {
        TEST_PARSING("location 1 { section l align 1 offset 2 {} }", "section label align offset")
    }
    SECTION("section label org") {
        TEST_PARSING("location 1 { section l org 1 {} }", "section org")
    }
    SECTION("section invalid: label align offset org") {
        TEST_PARSING_EXCEPTION("location 1 { section l align 1 offset 2 org 3 {} }", "section invalid: label align offset org")
    }
    SECTION("duplicate section label will be rejected") {
        TEST_PARSING_EXCEPTION("location 1 { section l {} section l {}}", "duplicate section label will be rejected")
    }

    SECTION("Keyword identification works") {
        TEST_PARSING_EXCEPTION("location 1 { section bne {} }", "section with keyword label")
    }
    SECTION("Labels with keyword as prefix works") {
        TEST_PARSING("location 1 { section bne1 {} }", "Label with keyword as prefix")
    }

    SECTION("varstart works") {
        TEST_PARSING("location $1000 to $1fff { varstart 1 section s1 {} }", "varstart")
    }
    SECTION("var works") {
        TEST_PARSING("location $1000 to $1fff { var test section s1 {} }", "var")
    }
    SECTION("var list works") {
        TEST_PARSING("location $1000 to $1fff { var test, test2, test3 section s1 {} }", "var list")
    }
    SECTION("var ranged works") {
        TEST_PARSING("location $1000 to $1fff { var test[2] section s1 {} }", "var ranged")
    }
    SECTION("var ranged list works") {
        TEST_PARSING("location $1000 to $1fff { var test[test], ptr[2] section s1 {} }", "var ranged list")
    }

    SECTION("const in location works") {
        TEST_PARSING("location $1000 to $1fff { test = 1 section s1 {} }", "const location")
    }
    SECTION("const in section works") {
        TEST_PARSING("location $1000 to $1fff { section s1 {test = 1} }", "const location")
    }

    SECTION("opcodes which can be imp or abs work") {
        TEST_PARSING("location 1 { section s1 { asl } }", "imp alone")
    }
    SECTION("opcodes which can be imp or abs work") {
        TEST_PARSING("location 1 { section s1 { asl a=1 } }", "imp + const decl")
    }
    SECTION("opcodes which can be imp or abs work") {
        TEST_PARSING("location 1 { section s1 { asl l: } }", "imp + label")
    }
}


////////////////////////////////////////////////////////////////////////////////
// Expressions
////////////////////////////////////////////////////////////////////////////////

int testExpr(std::string expr, std::string desc) {
    ParsingData data;
    TEST_PARSING(expr + " location 1 {}", desc)        
    data.resolveSymbols();
    data.exprParser.SetExpr("t");
    return int(data.exprParser.Eval());
}

TEST_CASE( "Expression parsing with muParserInt works", "[EXPRESSIONS]" ) {
    SECTION("numbers") {
        REQUIRE(testExpr("t = 100", "dec") == 100);
        REQUIRE(testExpr("t = %100", "bin") == 4);
        REQUIRE(testExpr("t = $100", "hex") == 256);
    }
    SECTION("add") {
        REQUIRE(testExpr("t = 100+%100+$100", "add") == 360);
    }
    SECTION("brackets") {
        REQUIRE(testExpr("t = 2+(3*4)", "brackets") == 14);
    }
    SECTION("xor") {
        REQUIRE(testExpr("t = %1010^%1111", "xor") == 5);
        REQUIRE(testExpr("t = %1010^%0", "xor") == 10);
    }
    SECTION("mod") {
        REQUIRE(testExpr("t = %1001%%101", "mod") == 4);
        REQUIRE(testExpr("t = $9%$2", "mod") == 1);
        REQUIRE(testExpr("t = 9%3", "mod") == 0);
    }
    SECTION("lo/hi byte") {
        REQUIRE(testExpr("t = <$0102", "lohi") == 2);
        REQUIRE(testExpr("t = >$0102", "lohi") == 1);
        REQUIRE(testExpr("t = <($0100+$02)", "lohi") == 2);
        REQUIRE(testExpr("t = >($0100+$02)", "lohi") == 1);
        REQUIRE(testExpr("a=$0102 t = <(>a+<a)", "lohi") == 3);
        REQUIRE_THROWS(testExpr("t = <$10000", "lohi"));
    }
    SECTION("constant derivative") {
        REQUIRE(testExpr("test=1 t=test+1", "constant derivative") == 2);
        REQUIRE(testExpr("test=1 t=1+test", "constant derivative") == 2);
    }
    SECTION("bit shift") {
        REQUIRE(testExpr("t = 7>>1", "bit shift") == 3);
        REQUIRE(testExpr("t = %1010>>2", "bit shift") == 2);
        REQUIRE(testExpr("t = 0<<5", "bit shift") == 0);
        REQUIRE(testExpr("t = 1<<3", "bit shift") == 8);
    }

}


////////////////////////////////////////////////////////////////////////////////
// Lexing
////////////////////////////////////////////////////////////////////////////////

TEST_CASE( "Lexing works", "[LEXING]" ) {
    ParsingData data;

    SECTION("AbsInd") {
        TEST_PARSING("location 1 { section a { jmp (1) } }", "AbsInd")
        REQUIRE( data.activeTokens.size() == 0);
        std::vector<int> instrTypes = sectionToTestVector(&data);
        REQUIRE( instrTypes.size() == 1);
        REQUIRE( instrTypes == std::vector<int>{ I_IND });        
    }
    SECTION("AbsInd Expression") {
        TEST_PARSING("location 1 { section a { jmp ((1+2)) } }", "AbsInd Expression")
        REQUIRE( data.activeTokens.size() == 0);
        std::vector<int> instrTypes = sectionToTestVector(&data);
        REQUIRE( instrTypes.size() == 1);
        REQUIRE( instrTypes == std::vector<int>{ I_IND });        
    }

    SECTION("Abs") {
        TEST_PARSING("location 1 { section a { jmp 1 } }", "Abs")
        REQUIRE( data.activeTokens.size() == 0);
        std::vector<int> instrTypes = sectionToTestVector(&data);
        REQUIRE( instrTypes.size() == 1);
        REQUIRE( instrTypes == std::vector<int>{ I_ABS });        
    }
    SECTION("nop abs") {
        TEST_PARSING("location 1 { section a { nop 1 } }", "Abs")
        REQUIRE( data.activeTokens.size() == 0);
        std::vector<int> instrTypes = sectionToTestVector(&data);
        REQUIRE( instrTypes.size() == 1);
        REQUIRE( instrTypes == std::vector<int>{ I_ABS });        
    }

    SECTION("Imp") {
        TEST_PARSING("location 1 { section a { nop } }", "Imp")
        REQUIRE( data.activeTokens.size() == 0);
        std::vector<int> instrTypes = sectionToTestVector(&data);
        REQUIRE( instrTypes.size() == 1);
        REQUIRE( instrTypes == std::vector<int>{ I_IMP });        
    }

    SECTION("Imp Imp no Abs") {
        TEST_PARSING("location 1 { section a { nop nop } }", "Imp")
        REQUIRE( data.activeTokens.size() == 0);
        std::vector<int> instrTypes = sectionToTestVector(&data);
        REQUIRE( instrTypes.size() == 2);
        std::vector<int> v{I_IMP, I_IMP};
        REQUIRE( instrTypes == v);        
    }

}

}}
