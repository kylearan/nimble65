#include "catch.hpp"

#include <string>
#include <iostream>
#include <vector>
#include <typeinfo>

#include <pegtl.hh>
#include <pegtl/analyze.hh>
#include <pegtl/trace.hh>

#include "logger.hpp"
#include "exceptions.hpp"
#include "parser/assembler/common.hpp"
#include "parser/assembler/grammar.hpp"
#include "lexer/location.hpp"
#include "lexer/section.hpp"
#include "lexer/instrdata.hpp"
#include "lexer/instrimp.hpp"
#include "lexer/instrimm.hpp"
#include "lexer/instrabs.hpp"
#include "lexer/instrabsx.hpp"
#include "lexer/instrabsy.hpp"
#include "lexer/instrrel.hpp"
#include "lexer/instrind.hpp"
#include "lexer/instrindx.hpp"
#include "lexer/instrindy.hpp"

#include "parsingdata.hpp"

#include "muParserInt.h"
#include "muParserBase.h"
#include "muParserError.h"

#include "lexer/symbol.hpp"

extern LogConfig logCfg;

namespace nimble65 { namespace tests {

void prepareForBinary(std::string src, std::string desc, ParsingData& data);
    
TEST_CASE("Generating binary works", "[BINARY]") {
    ParsingData data;
    int start = 0;
    
    SECTION("dc.b") {
        prepareForBinary("location $1000 to $1003 { section l org $1000 { dc.b 0 dc.b 1, 2, 3 } }", "dc.b", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0, 1, 2, 3};
        REQUIRE(bin == v);
    }   
    SECTION("Negative dc.b") {
        prepareForBinary("location $1000 to $1001 { section l org $1000 { dc.b -1, -128 } }", "negative dc.b", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{255, 128};
        REQUIRE(bin == v);
    }   
    SECTION("dc.w") {
        prepareForBinary("location $1000 to $1007 { section l org $1000 { dc.w 0, 1 dc.w 3*256+2, 5*256+4 } }", "dc.b", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0, 0, 1, 0, 2, 3, 4, 5};
        REQUIRE(bin == v);
    }   
    SECTION("Negative dc.w") {
        prepareForBinary("location $1000 to $1003 { section l org $1000 { dc.w -1, -32768 } }", "negative dc.w", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{255, 255, 0, 128};
        REQUIRE(bin == v);
    }   
    SECTION("Out of range dc.b") {
        prepareForBinary("location $1000 to $1003 { section l org $1000 { dc.b 256 } }", "out of range dc.b", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   
    SECTION("Out of range negative dc.b") {
        prepareForBinary("location $1000 to $1003 { section l org $1000 { dc.b -129 } }", "out of range negative dc.b", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   
    SECTION("Out of range dc.w") {
        prepareForBinary("location $1000 to $1003 { section l org $1000 { dc.w 65536 } }", "out of range dc.w", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   
    SECTION("Out of range negative dc.w") {
        prepareForBinary("location $1000 to $1003 { section l org $1000 { dc.w -32769 } }", "out of range negative dc.w", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   

    SECTION("InstrImp") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { nop CLI rTs } }", "InstrImp", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xea, 0x58, 0x60};
        REQUIRE(bin == v);
    }   

    SECTION("InstrImm") {
        prepareForBinary("location $1000 to $1003 { section l org $1000 { lda #1 ldx #-2 } }", "InstrImm", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xa9, 0x01, 0xa2, 0xfe };
        REQUIRE(bin == v);
    }   
    SECTION("Out of range InstrImm") {
        prepareForBinary("location $1000 to $1003 { section l org $1000 { lda #-129 } }", "out of range InstrImm", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   

    SECTION("InstrRel") {
        prepareForBinary("location $1000 to $1003 { section l org $1000 { l1: bne l1 beq l2 l2:} }", "InstrRel", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xd0, 0xfe, 0xf0, 0x00};
        REQUIRE(bin == v);
    }   
    SECTION("Out of range InstrRel") {
        prepareForBinary("location $1000 to $1100 { section l1 org $1000 { bne l2 } section l2 org $10f0 {}}", "out of range InstrRel", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   

    SECTION("InstrInd") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { jmp ($0102) } }", "InstrInd", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0x6c, 0x02, 0x01};
        REQUIRE(bin == v);
    }   
    SECTION("InstrInd jam") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { jam ($0102) } }", "InstrInd jam", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xd2, 0x02, 0x01};
        REQUIRE(bin == v);
    }   
    SECTION("Out of range InstrInd") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { jmp ($010203) } }", "out of range InstrInd", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   

    SECTION("InstrAbs ZP") {
        prepareForBinary("location $1000 to $1001 { section l org $1000 { lda $42 } }", "InstrAbs ZP", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xa5, 0x42};
        REQUIRE(bin == v);
    }   
    SECTION("InstrAbs word") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { lda $2342 } }", "InstrAbs word", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xad, 0x42, 0x23};
        REQUIRE(bin == v);
    }   
    SECTION("InstrAbs force word") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { lda.w $42 } }", "InstrAbs force word", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xad, 0x42, 0x00};
        REQUIRE(bin == v);
    }   
    SECTION("Out of range InstrAbs") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { lda $10000 } }", "Out of range InstrAbs", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   
    SECTION("InstrAbs jmp cannot be zp") {
        prepareForBinary("location $1000 to $1005 { section l org $1000 { jmp $01 jmp $0203 } }", "InstrAbs jmp cannot be zp", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0x4c, 0x01, 0x00, 0x4c, 0x03, 0x02};
        REQUIRE(bin == v);
    }   

    SECTION("InstrAbsX ZP") {
        prepareForBinary("location $1000 to $1001 { section l org $1000 { lda $42,x } }", "InstrAbsX ZP", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xb5, 0x42};
        REQUIRE(bin == v);
    }   
    SECTION("InstrAbsX word") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { lda $2342,x } }", "InstrAbsX word", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xbd, 0x42, 0x23};
        REQUIRE(bin == v);
    }   
    SECTION("InstrAbsX force word") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { lda.w $42,x } }", "InstrAbsX force word", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xbd, 0x42, 0x00};
        REQUIRE(bin == v);
    }   
    SECTION("Out of range InstrAbsX") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { lda $10000,x } }", "Out of range InstrAbsX", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   

    SECTION("InstrAbsY ZP") {
        prepareForBinary("location $1000 to $1001 { section l org $1000 { ldx $42,y } }", "InstrAbsY ZP", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xb6, 0x42};
        REQUIRE(bin == v);
    }   
    SECTION("InstrAbsY word") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { ldx $2342,y } }", "InstrAbsY word", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xbe, 0x42, 0x23};
        REQUIRE(bin == v);
    }   
    SECTION("InstrAbsY force word") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { ldx.w $42,y } }", "InstrAbsY force word", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xbe, 0x42, 0x00};
        REQUIRE(bin == v);
    }   
    SECTION("Out of range InstrAbsY") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { lda $10000,y } }", "Out of range InstrAbsX", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   
    SECTION("Invalid .w,y") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { stx.w 1,y } }", "Invalid .w,y", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   
    SECTION("Invalid .w,y") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { stx 1000,y } }", "Invalid .w,y", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   
    SECTION("Valid zp,y") {
        prepareForBinary("location $1000 to $1001 { section l org $1000 { stx $42,y } }", "InstrAbsY ZP", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0x96, 0x42};
        REQUIRE(bin == v);
    }   

    SECTION("InstrIndX") {
        prepareForBinary("location $1000 to $1001 { section l org $1000 { lda ($42,x) } }", "InstrIndX", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xa1, 0x42};
        REQUIRE(bin == v);
    }   
    SECTION("Out of range InstrIndX") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { lda (1000,x) } }", "Out of range InstrIndX", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   

    SECTION("InstrIndY") {
        prepareForBinary("location $1000 to $1001 { section l org $1000 { lax ($42),y } }", "InstrIndY", data);
        auto bin = data.generateBinary(start);
        std::vector<unsigned char> v{0xb3, 0x42};
        REQUIRE(bin == v);
    }   
    SECTION("Out of range InstrIndY") {
        prepareForBinary("location $1000 to $1002 { section l org $1000 { lax (1000),y } }", "Out of range InstrIndY", data);
        REQUIRE_THROWS(data.generateBinary(start));
    }   

}

}}