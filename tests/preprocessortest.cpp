#include "catch.hpp"

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <typeinfo>

#include <pegtl.hh>
#include <pegtl/analyze.hh>
#include <pegtl/trace.hh>

#include <experimental/filesystem>

#include "nimble65.hpp"
#include "n65.hpp"
#include "logger.hpp"
#include "exceptions.hpp"
#include "preprocessor/pp_common.hpp"
#include "preprocessor/pp_grammar.hpp"
#include "parser/assembler/common.hpp"
#include "parser/assembler/grammar.hpp"
#include "lexer/location.hpp"
#include "lexer/section.hpp"
#include "lexer/instrdata.hpp"
#include "lexer/instrimp.hpp"
#include "lexer/instrimm.hpp"
#include "lexer/instrabs.hpp"
#include "lexer/instrabsx.hpp"
#include "lexer/instrabsy.hpp"
#include "lexer/instrrel.hpp"
#include "lexer/instrind.hpp"
#include "lexer/instrindx.hpp"
#include "lexer/instrindy.hpp"

#include "parsingdata.hpp"

#include "muParserInt.h"
#include "muParserBase.h"
#include "muParserError.h"

#include "lexer/symbol.hpp"

extern LogConfig logCfg;

namespace nimble65 { namespace tests {

    // Test a PEGTL parsing run with S a string to be parsed and T the test name
#define TEST_PARSING(S, T) \
{ \
    bool result = pegtl::parse<nimble65::parser::preprocessor::pp_grammar, nimble65::parser::preprocessor::action, nimble65::parser::preprocessor::errors>(S, T, data); \
    REQUIRE(result); \
}

// Test if a PEGTL parsing run throws an exception
#define TEST_PARSING_EXCEPTION(S, T) \
{ \
    bool result = true; \
    try { \
        pegtl::parse<nimble65::parser::preprocessor::pp_grammar, nimble65::parser::preprocessor::action, nimble65::parser::preprocessor::errors>(S, T, data); \
        } catch (...) { result = false; } \
    REQUIRE(!result); \
}

#define TEST_PARSING_NOTHROW(S, T) \
{ \
bool result = true; \
try { \
    pegtl::parse<nimble65::parser::preprocessor::pp_grammar, nimble65::parser::preprocessor::action, nimble65::parser::preprocessor::errors>(S, T, data); \
    } catch (...) { result = false; } \
REQUIRE(result); \
}

void writeTestSource(std::string filename, std::string src) {
    std::ofstream out;
    out.open(filename);
    out << src;
    out.close();
}

TEST_CASE("include works", "[INCLUDE]") {
    ParsingData data;
    NimbleConfig conf;
    conf.inFilename = "test.asm";
    N65 n65(conf);

    SECTION("No include") {
        std::string t("location $1000 { section s { nop } }");
        TEST_PARSING(t, "no preprocessing");
        REQUIRE(t == data.preprocessedSource);
    }
    SECTION("include in comment") {
        std::string t("; #include \"test\"\nlocation $1000 { section s { nop } }");
        TEST_PARSING(t, "include in comment");
        REQUIRE(t == data.preprocessedSource);
    }
    SECTION("include at beginning") {
        std::string i("; this got included\n");
        writeTestSource("i.asm", i);
        std::string inc("#include \"i.asm\"");
        std::string t(" location $1000 { section s { nop } }");
        TEST_PARSING(inc + t, "include in comment");
        REQUIRE(i + t == data.preprocessedSource);
    }
    SECTION("include in include") {
        std::string i1("; this is i1\n#include \"i2.asm\"\n;Line after include i2\n");
        writeTestSource("i1.asm", i1);
        std::string i2("; this is i2\n");
        writeTestSource("i2.asm", i2);
        std::string inc("#include \"i1.asm\"\n");
        std::string t("location $1000 { section s { nop } }");
        writeTestSource("test.asm", inc + t);
        bool result = n65.preprocessSource(data);
        REQUIRE("; this is i1\n; this is i2\n\n;Line after include i2\n\n" + t == data.preprocessedSource);
    }
    SECTION("include non-existant file") {
        writeTestSource("test.asm", "#include \"doesnotexist\" location $1000 { section s { nop } }");
        REQUIRE(!n65.preprocessSource(data));
    }
}

}}
