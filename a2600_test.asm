; Test program for assemble65

include "atari2600.h"

TIM_OVERSCAN    = 50    ; TIM64T,  3200 cycles = ~ 42 scanlines
TIM_VBLANK      = 61    ; TIM64T,  3904 cycles = ~ 51 scanlines
TIM_KERNEL      = 17    ; T1024T, 17408 cycles = ~229 scanlines

var numFrame
var ptr[2]


location $f000-$ffff {
    section waitForIntim {
        lda INTIM
        bne waitForIntim
        rts
    }

    section doOverscan {
        sta WSYNC
        lda #2
        sta VBLANK
        lda #TIM_OVERSCAN
        sta TIM64T

        jsr waitForIntim
    }

    section doVBlank {
        lda #%1110
    .vsyncLoop:
        sta WSYNC
        sta VSYNC
        lsr
        bne .vsyncLoop
        lda #2
        sta VBLANK
        lda #TIM_VBLANK
        sta TIM64T

        jsr waitForIntim
    }

    section doKernel {
        lda #TIM_KERNEL
        sta T1024T

        jsr waitForIntim
    }

    section start {
        ; Clear zeropage
        cld
        ldx #0
        txa
    .clearLoop:
        dex
        tsx
        pha
        bne .clearLoop

    mainLoop:
        jsr doOverscan
        jsr doVBlank
        jsr doKernel
        jmp mainLoop
    }

    section vectors org $fffc {
        word start, start
    }
}
