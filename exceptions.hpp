#pragma once

#include <stdexcept>

namespace nimble65 {

class compile_error : public std::runtime_error {
    public:
        compile_error(const int line, const int column, const std::string & message) :
            std::runtime_error(message),
            line(line),
            column(column) {};

    const int line;
    const int column;
};

class link_error : public std::runtime_error {
    public:
        link_error(const int line, const int column, const std::string & message) :
            std::runtime_error(message),
            line(line),
            column(column) {};

        static std::string toHex(int value) {
            std::stringstream stream;
            stream << std::hex << value;
            return stream.str();
        }
            
    const int line;
    const int column;
};

}
