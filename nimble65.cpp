
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include <pegtl.hh>
#include <pegtl/analyze.hh>
#include <pegtl/trace.hh>

#include "nimble65.hpp"
#include "n65.hpp"
#include "logger.hpp"
#include "exceptions.hpp"
#include "preprocessor/pp_common.hpp"
#include "preprocessor/pp_grammar.hpp"
#include "parser/assembler/common.hpp"
#include "parser/assembler/grammar.hpp"
#include "lexer/location.hpp"
#include "lexer/section.hpp"
#include "lexer/instrdata.hpp"
#include "lexer/instrimp.hpp"
#include "lexer/instrimm.hpp"
#include "lexer/instrabs.hpp"
#include "lexer/instrabsx.hpp"
#include "lexer/instrabsy.hpp"
#include "lexer/instrrel.hpp"
#include "lexer/instrind.hpp"
#include "lexer/instrindx.hpp"
#include "lexer/instrindy.hpp"

#include "parsingdata.hpp"

#include "muParserInt.h"
#include "muParserBase.h"
#include "muParserError.h"

#include "lexer/symbol.hpp"

#include "args.hxx"

LogConfig logCfg;

using namespace nimble65;

/* Parse arguments and fill in a NimbleConfig.
 */
NimbleConfig parseArgs(int argc, char *argv[])
{
    NimbleConfig conf;
    args::ArgumentParser parser("An assembler and linker for 650x assembly.",
                                "Version 1.0 by Andre Wichmann <andre.wichmann@gmx.de>");
    parser.Prog("nimble65");
    args::HelpFlag help(parser, "help", "Display this help", {'h', "help"});
    args::ValueFlag<int> verbosity(parser, "verbosity", "Verbosity level (0-3)", {'v'});
    args::ValueFlag<std::string> outfile(parser, "outfile", "Filename for the assembled binary file (default: sourcefile with .bin extension)", {'o'});
    args::ValueFlag<std::string> symfile(parser, "symfile", "Filename for a Stella-compatible .sym-file", {'s'});
    args::ValueFlag<int> fillbyte(parser, "fillbyte", "Byte value to fill gaps between sections and locations (defualt: 0)", {'f'});
    args::Positional<std::string> sourcefile(parser, "sourcefile", "The 650x source file to assemble");
    try
    {
        parser.ParseCLI(argc, argv);
    }
    catch (args::Help)
    {
        std::cout << parser;
        std::exit(1);
    }
    catch (args::ParseError e)
    {
        std::cerr << e.what() << std::endl
                  << parser;
        std::exit(1);
    }
    catch (args::ValidationError e)
    {
        std::cerr << e.what() << std::endl
                  << parser;
        std::exit(1);
    }

    // Verbosity
    logCfg.headers = true;
    if (verbosity)
    {
        switch (args::get(verbosity))
        {
        case 0:
            logCfg.level = LogLevel::SILENT;
            break;
        case 2:
            logCfg.level = LogLevel::VERBOSE;
            break;
        case 3:
            logCfg.level = LogLevel::DEBUG;
            break;
        default:
            logCfg.level = LogLevel::INFO;
        }
    }
    else
    {
        logCfg.level = LogLevel::DEBUG;
    }

    // Source filename
    if (!sourcefile)
    {
        std::cerr << "Missing source filename!" << std::endl
                  << parser;
        std::exit(1);
    }
    conf.inFilename = args::get(sourcefile);

    // Outfile name
    if (outfile)
    {
        conf.outFilename = args::get(outfile);
    }
    else
    {
        // Remove potential extensiomn from source filename and add ".bin"
        size_t suffixIndex = conf.inFilename.find_last_of(".");
        if (suffixIndex == std::string::npos)
        {
            suffixIndex = conf.inFilename.size();
        }
        conf.outFilename = conf.inFilename.substr(0, suffixIndex);
        conf.outFilename.append(".bin");
    }
    LOG(LogLevel::DEBUG) << "Using outfile name: " << conf.outFilename;

    // Symbol filename
    if (symfile)
    {
        conf.symFilename = args::get(symfile);
        conf.writeSymFile = true;
        LOG(LogLevel::DEBUG) << "Writing symfile to: " << conf.symFilename;
    }

    // Fill byte
    if (fillbyte)
    {
        conf.fillByte = args::get(fillbyte);
    }
    LOG(LogLevel::DEBUG) << "Using fillbyte: " << (unsigned int)conf.fillByte;

    return conf;
}

/* Main
 */
int main(int argc, char *argv[])
{
    NimbleConfig conf = parseArgs(argc, argv);
    ParsingData data;
    data.fillByte = conf.fillByte;
    N65 n65(conf);

    if (!n65.preprocessSource(data))
    {
        return 1;
    }
    if (!n65.parseSource(data))
    {
        return 1;
    }
    if (data.locationDefs.size() == 0)
    {
        LOG(LogLevel::INFO) << "No locations defined, nothing to do.";
        return 0;
    }

    if (!n65.resolveSymbolsAndLink(data))
    {
        return 1;
    }

    if (!n65.generateAndWriteBinary(data))
    {
        return 1;
    }

    LOG(LogLevel::VERBOSE) << "Finished";
}
