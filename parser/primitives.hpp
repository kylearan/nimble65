#pragma once

#include <string>
#include <iostream>

#include <pegtl.hh>

namespace nimble65 { namespace parser
{
    using namespace pegtl;

    // Whitespace in one line of arbitrary length
    struct ws
        : star< blank > {};

    struct comment_delimeter
        : one< ';' > {};

    // Hash
    struct hash
        : one< '#' > {};

}}