#pragma once

#include <string>
#include <iostream>

#include <pegtl.hh>

namespace nimble65 { namespace parser
{
    using namespace pegtl;
    // Preprocessor
    struct pp_include : pegtl_istring_t("include") {};

    // Parser
    struct varstart : pegtl_istring_t("varstart") {};
    struct var : pegtl_istring_t("var") {};
    struct section : pegtl_istring_t("section") {};
    struct align : pegtl_istring_t("align") {};
    struct offset : pegtl_istring_t("offset") {};
    struct org : pegtl_istring_t("org") {};
    struct samepage : pegtl_istring_t("samepage") {};
    struct crosspage : pegtl_istring_t("crosspage") {};
    struct location : pegtl_istring_t("location") {};
    struct to : pegtl_istring_t("to") {};

    struct dcb : pegtl_istring_t("dc.b") {};
    struct dcw : pegtl_istring_t("dc.w") {};

    struct asl : pegtl_istring_t("asl") {};
    struct brk : pegtl_istring_t("brk") {};
    struct clc : pegtl_istring_t("clc") {};
    struct cld : pegtl_istring_t("cld") {};
    struct cli : pegtl_istring_t("cli") {};
    struct clv : pegtl_istring_t("clv") {};
    struct dex : pegtl_istring_t("dex") {};
    struct dey : pegtl_istring_t("dey") {};
    struct inx : pegtl_istring_t("inx") {};
    struct iny : pegtl_istring_t("iny") {};
    struct lsr : pegtl_istring_t("lsr") {};
    struct nop : pegtl_istring_t("nop") {};
    struct pha : pegtl_istring_t("pha") {};
    struct php : pegtl_istring_t("php") {};
    struct pla : pegtl_istring_t("pla") {};
    struct plp : pegtl_istring_t("plp") {};
    struct rol : pegtl_istring_t("rol") {};
    struct ror : pegtl_istring_t("ror") {};
    struct rti : pegtl_istring_t("rti") {};
    struct rts : pegtl_istring_t("rts") {};
    struct sec : pegtl_istring_t("sec") {};
    struct sei : pegtl_istring_t("sei") {};
    struct tax : pegtl_istring_t("tax") {};
    struct tay : pegtl_istring_t("tay") {};
    struct tsx : pegtl_istring_t("tsx") {};
    struct txa : pegtl_istring_t("txa") {};
    struct txs : pegtl_istring_t("txs") {};
    struct tya : pegtl_istring_t("tya") {};
    struct jam : pegtl_istring_t("jam") {};
    struct lda : pegtl_istring_t("lda") {};
    struct ldx : pegtl_istring_t("ldx") {};
    struct ldy : pegtl_istring_t("ldy") {};
    struct cmp : pegtl_istring_t("cmp") {};
    struct cpx : pegtl_istring_t("cpx") {};
    struct cpy : pegtl_istring_t("cpy") {};
    struct ora : pegtl_istring_t("ora") {};
    struct iand : pegtl_istring_t("and") {};
    struct eor : pegtl_istring_t("eor") {};
    struct adc : pegtl_istring_t("adc") {};
    struct sbc : pegtl_istring_t("sbc") {};
    struct anc : pegtl_istring_t("anc") {};
    struct alr : pegtl_istring_t("alr") {};
    struct arr : pegtl_istring_t("arr") {};
    struct sbx : pegtl_istring_t("sbx") {};
    struct lax : pegtl_istring_t("lax") {};
    struct ane : pegtl_istring_t("ane") {};
    struct jmp : pegtl_istring_t("jmp") {};
    struct jsr : pegtl_istring_t("jsr") {};
    struct bit : pegtl_istring_t("bit") {};
    struct dec : pegtl_istring_t("dec") {};
    struct inc : pegtl_istring_t("inc") {};
    struct sta : pegtl_istring_t("sta") {};
    struct sty : pegtl_istring_t("sty") {};
    struct stx : pegtl_istring_t("stx") {};
    struct slo : pegtl_istring_t("slo") {};
    struct rla : pegtl_istring_t("rla") {};
    struct sre : pegtl_istring_t("sre") {};
    struct rra : pegtl_istring_t("rra") {};
    struct sax : pegtl_istring_t("sax") {};
    struct dcp : pegtl_istring_t("dcp") {};
    struct isc : pegtl_istring_t("isc") {};
    struct shy : pegtl_istring_t("shy") {};
    struct sha : pegtl_istring_t("sha") {};
    struct shx : pegtl_istring_t("shx") {};
    struct tas : pegtl_istring_t("tas") {};
    struct las : pegtl_istring_t("las") {};
    struct bpl : pegtl_istring_t("bpl") {};
    struct bmi : pegtl_istring_t("bmi") {};
    struct bvc : pegtl_istring_t("bvc") {};
    struct bvs : pegtl_istring_t("bvs") {};
    struct bcc : pegtl_istring_t("bcc") {};
    struct bcs : pegtl_istring_t("bcs") {};
    struct bne : pegtl_istring_t("bne") {};
    struct beq : pegtl_istring_t("beq") {};

    struct wide : pegtl_istring_t(".w") {};

    namespace assembler { struct comment; }
    struct keyword
        : seq< sor<
            varstart, var, section, align, offset, org, samepage, crosspage, location, to,
            dcb, dcw,
            asl, brk, clc, cld, cli, clv, dex, dey, inx, iny, lsr, nop, pha, php, pla, plp,
            rol, ror, rti, rts, sec, sei, tax, tay, tsx, txa, txs, tya, jam, lda, ldx, ldy,
            cmp, cpx, cpy, ora, iand, eor, adc, sbc, anc, alr, arr, sbx, lax, ane, jmp, jsr,
            bit, dec, inc, sta, sty, stx, slo, rla, sre, rra, sax, dcp, isc, shy, sha, shx,
            tas, las, bpl, bmi, bvc, bvs, bcc, bcs, bne, beq,
            wide
            >, sor< space, assembler::comment >
        > {};

}}
