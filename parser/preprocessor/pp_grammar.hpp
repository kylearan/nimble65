#pragma once

#include <string>
#include <iostream>
#include <fstream>

#include <pegtl.hh>
#include "preprocessor/pp_common.hpp"
#include "parser/assembler/common.hpp"

namespace nimble65
{
namespace parser
{
namespace preprocessor
{
using namespace pegtl;

//////////////////////////////////////////////////////////////////////
// Main grammar
//////////////////////////////////////////////////////////////////////

// A non-preprocessor portion of source that can be passed through
struct pp_src
    : sor<comment, any>
{
};
template <>
struct action<pp_src>
{
    static void apply(const input &in, ParsingData &data)
    {
        data.preprocessedSource.append(in.string());
    }
};

// Include
struct filename
    : seq<quote, plus<sor<alnum, one<' ', ':', '_', '.', '-', '+', '!', '/', '\\', ','>>>, must<quote>>
{
};
template <>
struct action<filename>
{
    static void apply(const input &in, ParsingData &data)
    {
        std::string f = in.string();
        data.filename = f.substr(1, f.size() - 2);
    }
};

struct inc_decl
    : seq<hash, parser::pp_include, wsc, must<filename>>
{
};
template <>
const std::string errors<filename>::error_message = "path to a file expected";
template <>
struct action<inc_decl>
{
    static void apply(const input &in, ParsingData &data)
    {
        LOG(LogLevel::DEBUG) << "Including file " + data.filename;
        std::ifstream incStream(data.filename);
        if (!incStream.is_open())
        {
            throw parse_error("Unable to open file \"" + data.filename + "\"", in);
        }
        std::stringstream incBuffer;
        incBuffer << incStream.rdbuf();
        data.preprocessedSource.append(incBuffer.str());
        // Adjust position mapping table
        int pos = in.begin() - data.curSrc.data();
        data.insertPosInfo(
            "included file " + data.filename,
            pos,
            incBuffer.str().size(),
            in.size(),
            in.line());
    }
};

struct pp_grammar
    : seq<plus<sor<inc_decl, pp_src>>, must<eof>>
{
};
template <>
const std::string errors<eof>::error_message = "end of file expected";

} // namespace preprocessor
} // namespace parser
} // namespace nimble65
