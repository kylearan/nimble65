#pragma once

#include <string>
#include <iostream>

#include <pegtl.hh>

#include "primitives.hpp"
#include "logger.hpp"
#include "lexer/token.hpp"
#include "lexer/identifier.hpp"
#include "lexer/symbol.hpp"
#include "parsingdata.hpp"
#include "parser/assembler/common.hpp"
#include "parser/keywords.hpp"

namespace nimble65
{
namespace parser
{
namespace preprocessor
{
using namespace pegtl;

//////////////////////////////////////////////////////////////////////
// Common building blocks
//////////////////////////////////////////////////////////////////////

// Basic struct for parsing error reporting
template <typename Rule>
struct errors
    : public normal<Rule>
{
    static const std::string error_message;

    template <typename Input, typename... States>
    static void raise(const Input &in, States &&...)
    {
        throw parse_error(error_message, in);
    }
};

// Base struct for actions
template <typename Rule>
struct action
    : nothing<Rule>
{
};

// Body open and close
struct body_open
    : pegtl_string_t("{")
{
};
template <>
const std::string errors<body_open>::error_message = "'{' expected";
struct body_close
    : pegtl_string_t("}")
{
};
template <>
const std::string errors<body_close>::error_message = "'}' expected";

// Quote
struct quote
    : one<'"'>
{
};
template <>
const std::string errors<quote>::error_message = "\" expected";

// Comments
struct comment
    : if_must<comment_delimeter, until<eolf>>
{
};
template <>
const std::string errors<until<eolf>>::error_message = "end of line expected";
// Whitespace including comments potentially spanning multiple lines
struct wsc
    : star<sor<space, comment>>
{
};

} // namespace preprocessor
} // namespace parser
} // namespace nimble65
