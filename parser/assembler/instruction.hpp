#pragma once

#include <string>
#include <iostream>

#include <pegtl.hh>

#include "primitives.hpp"
#include "common.hpp"
#include "expression.hpp"
#include "varconst.hpp"
#include "instrdata.hpp"
#include "instrimp.hpp"
#include "instrimm.hpp"
#include "instrabs.hpp"
#include "instrabsx.hpp"
#include "instrabsy.hpp"
#include "instrrel.hpp"
#include "instrind.hpp"
#include "instrindx.hpp"
#include "instrindy.hpp"


namespace nimble65 { namespace parser { namespace assembler
{
    using namespace pegtl;

    //////////////////////////////////////////////////////////////////////
    // Instructions
    //////////////////////////////////////////////////////////////////////

    // Data
    struct dcb_decl
        : seq< expr > {};
    template<> const std::string errors< dcb_decl >::error_message = "dc.b expression expecred";
    template<> struct action< dcb_decl >
    {
        static void apply( const input & in, ParsingData & data )
        {
            std::string expr = data.popExpression();
            int pos = in.begin() - data.preprocessedSource.data();
            data.addNewInstruction(new lexer::InstrData(in.line(), in.column(), pos,
                "dc.b", expr, 1));
            LOG(LogLevel::DEBUG) << "DC.B DATA at " << in.line() << ", " << in.column() << ": " << expr;
        }
    };

    struct dcb_list_decl
        : seq< dcb, wsc, must< dcb_decl >, wsc, star< one< ',' >, wsc, must< dcb_decl >, wsc > > {};

    struct dcw_decl
        : seq< expr > {};
    template<> const std::string errors< dcw_decl >::error_message = "dc.w expression expecred";
    template<> struct action< dcw_decl >
    {
        static void apply( const input & in, ParsingData & data )
        {
            std::string expr = data.popExpression();
            int pos = in.begin() - data.preprocessedSource.data();
            data.addNewInstruction(new lexer::InstrData(in.line(), in.column(), pos,
                "dc.w", expr, 2));
            LOG(LogLevel::DEBUG) << "DC.W DATA at " << in.line() << ", " << in.column() << ": " << expr;
        }
    };

    struct dcw_list_decl
        : seq< dcw, wsc, must< dcw_decl >, wsc, star< one< ',' >, wsc, must< dcw_decl >, wsc > > {};

    // Imperative instructions
    struct instr_imp
        : seq< sor< asl, brk, clc, cld, cli, clv, dex, dey, inx, iny, lsr, nop,
            pha, pla, plp, rol, ror, rti, rts, sec, sei, tax, tay, tsx, txa,
            txs, tya, jam >, sor< space, comment > > {};
    template<> struct action< instr_imp >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // We know the mnemonic is exactly 3 letters long. in.string()
            // might contain a newline, so cut it
            std::string instr = in.string().substr(0, 3);
            int pos = in.begin() - data.preprocessedSource.data();
            data.addNewInstruction(new lexer::InstrImp(in.line(), in.column(), pos, instr));
            LOG(LogLevel::DEBUG) << "INSTR_IMP: " << instr;
        }
    };

    // Immediate instructions
    struct instr_imm
        : seq< sor< lda, ldx, ldy, cmp, cpx, cpy, ora, iand, eor, adc, sbc,
            anc, alr, arr, sbx, lax, ane, nop, jam >,
            wsc, hash, must< expr > > {};
    template<> struct action< instr_imm >
    {
        static void apply( const input & in, ParsingData & data )
        {
            auto expr = data.popExpression();
            // We know the mnemonic is exactly 3 letters long
            std::string instr = in.string().substr(0, 3);
            int pos = in.begin() - data.preprocessedSource.data();
            data.addNewInstruction(new lexer::InstrImm(in.line(), in.column(), pos, instr, expr));
            LOG(LogLevel::DEBUG) << "INSTR_IMM: " << instr << " #" << expr;
        }
    };

    // Indirect instructions
    struct instr_ind
        : seq < sor< jmp, jam >, wsc, one< '(' >, wsc, must< expr >, wsc, must< one< ')' > > > {};
    template<> const std::string errors< one< ')' > >::error_message = "')' expected";
    template<> struct action< instr_ind >
    {
        static void apply( const input & in, ParsingData & data )
        {
            auto expr = data.popExpression();
            // We know the mnemonic is exactly 3 letters long
            std::string instr = in.string().substr(0, 3);
            int pos = in.begin() - data.preprocessedSource.data();
            data.addNewInstruction(new lexer::InstrInd(in.line(), in.column(), pos, instr, expr));
            LOG(LogLevel::DEBUG) << "INSTR_IND: " << instr << " (" << expr << ")";
        }
    };

    // Absolute instructions
    struct abs_mnemonic
        : sor< jsr, bit, jmp, sty, ldy, cpy, cpx, ora, iand, eor, adc,
        sta, lda, cmp, sbc, asl, rol, lsr, ror, stx, ldx, dec, inc,
        nop, slo, rla, sre, rra, sax, lax, dcp, isc, jam > {};
    struct instr_abs
        : seq< sor< seq< abs_mnemonic, wide >, abs_mnemonic >,
        wsc, expr, not_at< one< ':' > >, not_at< one< '=' > > > {};
    template<> struct action< instr_abs >
    {
        static void apply( const input & in, ParsingData & data )
        {
            auto expr = data.popNonBracketedExpression(in);
            // We know the mnemonic is exactly 3 letters long, or 5 for .w
            std::string instr = in.string().substr(0, 3);
            int pos = in.begin() - data.preprocessedSource.data();
            data.addNewInstruction(new lexer::InstrAbs(in.line(), in.column(), pos,
                instr, expr, (in.string().size() > 3 && in.string()[3] == '.')));
            LOG(LogLevel::DEBUG) << "INSTR_ABS: " << instr << " " << expr;
        }
    };

    // Absolute,x instructions
    struct commax : seq< one< ',' >, sor< one< 'x' >, one< 'X' > > > {};
    struct absx_mnemonic
        : sor< ldy, ora, iand, eor, adc, sta, lda, cmp, sbc, asl, rol,
        lsr, ror, dec, inc,
        slo, rla, sre, rra, dcp, isc, shy, nop, jam > {};
    struct instr_absx
        : seq< sor< seq< absx_mnemonic, wide >, absx_mnemonic >,
        wsc, expr, commax > {};
    template<> struct action< instr_absx >
    {
        static void apply( const input & in, ParsingData & data )
        {
            auto expr = data.popNonBracketedExpression(in);
            // We know the mnemonic is exactly 3 letters long, or 5 for .w
            std::string instr = in.string().substr(0, 3);
            int pos = in.begin() - data.preprocessedSource.data();
            data.addNewInstruction(new lexer::InstrAbsX(in.line(), in.column(), pos,
                instr, expr,  (in.string().size() > 3 && in.string()[3] == '.')));
            LOG(LogLevel::DEBUG) << "INSTR_ABS,X: " << instr << " " << expr << ",x";
        }
    };

    // Absolute,y instructions
    struct commay : seq< one< ',' >, sor< one< 'y' >, one< 'Y' > > > {};
    struct absy_mnemonic
        : sor< ora, iand, eor, adc, sta, lda, cmp, sbc, ldx, stx,
        slo, rla, sre, rra, lax, sax, dcp, isc, sha, shx, tas, las, jam > {};
    struct instr_absy
        : seq< sor< seq< absy_mnemonic, wide >, absy_mnemonic >,
        wsc, expr, commay > {};
    template<> struct action< instr_absy >
    {
        static void apply( const input & in, ParsingData & data )
        {
            auto expr = data.popNonBracketedExpression(in);
            // We know the mnemonic is exactly 3 letters long, or 5 for .w
            std::string instr = in.string().substr(0, 3);
            int pos = in.begin() - data.preprocessedSource.data();
            data.addNewInstruction(new lexer::InstrAbsY(in.line(), in.column(), pos,
                instr, expr,  (in.string().size() > 3 && in.string()[3] == '.')));
            LOG(LogLevel::DEBUG) << "INSTR_ABS,Y: " << instr << " " << expr << ",y";
        }
    };

    // (indirect,x) instructions
    struct instr_indx
        : seq < sor< ora, iand, eor, adc, sta, lda, cmp, sbc,
        slo, rla, sre, rra, sax, lax, dcp, isc, jam >,
        wsc, one< '(' >, wsc, must< expr >, commax, wsc, must< one< ')' > > > {};
    template<> struct action< instr_indx >
    {
        static void apply( const input & in, ParsingData & data )
        {
            auto expr = data.popExpression();
            // We know the mnemonic is exactly 3 letters long
            std::string instr = in.string().substr(0, 3);
            int pos = in.begin() - data.preprocessedSource.data();
            data.addNewInstruction(new lexer::InstrIndX(in.line(), in.column(), pos, instr, expr));
            LOG(LogLevel::DEBUG) << "INSTR_IND,X: " << instr << " (" << expr << ",x)";
        }
    };

    // (indirect),y instructions
    struct instr_indy
        : seq < sor< ora, iand, eor, adc, sta, lda, cmp, sbc,
        slo, rla, sre, rra, lax, dcp, isc, sha, jam >,
        wsc, one< '(' >, wsc, must< expr >, wsc, must< one< ')' > >, commay > {};
    template<> struct action< instr_indy >
    {
        static void apply( const input & in, ParsingData & data )
        {
            auto expr = data.popExpression();
            // We know the mnemonic is exactly 3 letters long
            std::string instr = in.string().substr(0, 3);
            int pos = in.begin() - data.preprocessedSource.data();
            data.addNewInstruction(new lexer::InstrIndY(in.line(), in.column(), pos, instr, expr));
            LOG(LogLevel::DEBUG) << "INSTR_IND,Y: " << instr << " (" << expr << "),y";
        }
    };

    // Relative instructions
    struct instr_rel
        : seq < sor< bpl, bmi, bvc, bvs, bcc, bcs, bne, beq >,
        wsc, must< expr > > {};
    template<> struct action< instr_rel >
    {
        static void apply( const input & in, ParsingData & data )
        {
            auto expr = data.popExpression();
            // We know the mnemonic is exactly 3 letters long
            std::string instr = in.string().substr(0, 3);
            auto section = data.getCurrentSection();
            // +2+ because we need to calc address *after* the branch instruction
            std::string relExpr = std::string(expr) + "-(" + section->label + "+2+" + section->getSizeExpr() + ")";
            int pos = in.begin() - data.preprocessedSource.data();
            data.addNewInstruction(new lexer::InstrRel(in.line(), in.column(), pos, instr, relExpr));
            LOG(LogLevel::DEBUG) << "INSTR_REL: " << instr << " " << relExpr;
        }
    };

    // Putting it all together
    struct instruction
        : sor< dcb_list_decl, dcw_list_decl,
        instr_imm, instr_indx, instr_indy, instr_ind,
        instr_absx, instr_absy, instr_abs, instr_imp, instr_rel > {};
}}}
