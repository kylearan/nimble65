#pragma once

#include <string>
#include <iostream>

#include <pegtl.hh>

#include "common.hpp"
#include "expression.hpp"
#include "varconst.hpp"
#include "section_body.hpp"


namespace nimble65 { namespace parser { namespace assembler
{
    using namespace pegtl;

    //////////////////////////////////////////////////////////////////////
    // section LABEL (align EXPR (offset EXPR) | org EXPR) { SECTION_BOY }
    //////////////////////////////////////////////////////////////////////

    // "offset" inserts a dummy token to protect the align expression. Two
    // xpressions in a row are not allowed and will be removed by the expression
    // rules.
    template<> struct action< offset >
    {
        static void apply( const input & in, ParsingData & data )
        {
            auto i = new lexer::Identifier(in.line(), in.column(), "offset");
            data.activeTokens.push_back(std::unique_ptr<lexer::Identifier>(i));
        }
    };

    // align declaration
    struct align_decl
        : seq< align, wsc, must< expr > > {};

    // offset declaration
    struct offset_decl
        : seq< offset, wsc, must< expr > > {};

    // align offset declatation
    struct align_offset_decl
        : seq< align_decl, wsc, offset_decl > {};

    // org declaration
    struct org_decl
        : seq< org, wsc, must< expr > > {};

    // section declaration variants
    struct section_align_offset
        : seq< section, wsc, name, wsc, align_offset_decl > {};
    template<> struct action< section_align_offset >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // The last three tokens are the section identifier, the align
            // expression and the offset expression
            std::string expr2 = data.popExpression();
            data.popIdentifier();   // Remove dummy "offset" identifier
            std::string expr1 = data.popExpression();
            std::string ident = data.popIdentifier();
            // Create new section and push it to current location's sections
            data.addNewSection(new lexer::Section(in.line(), in.column(), ident, expr1, true, expr2));
            LOG(LogLevel::DEBUG) << "SECTION_ALIGN_offset at " << in.line() << ", " << in.column() << ": " << ident << " align " << expr1 << " offset " << expr2;
        }
    };

    struct section_align
        : seq< section, wsc, name, wsc, align_decl > {};
    template<> struct action< section_align >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // The last two tokens are the section identifier and the align
            // expression
            std::string expr = data.popExpression();
            std::string ident = data.popIdentifier();
            // Create new section and push it to current location's sections
            data.addNewSection(new lexer::Section(in.line(), in.column(), ident, expr, false));
            LOG(LogLevel::DEBUG) << "SECTION_ALIGN at " << in.line() << ", " << in.column() << ": " << ident << " align " << expr;
        }
    };

    struct section_org
        : seq< section, wsc, name, wsc, org_decl > {};
    template<> struct action< section_org >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // The last two tokens are the section identifier and the org
            // expression
            std::string expr = data.popExpression();
            std::string ident = data.popIdentifier();
            // Create new section and push it to current location's sections
            data.addNewSection(new lexer::Section(in.line(), in.column(), ident, expr));
            LOG(LogLevel::DEBUG) << "SECTION_ORG at " << in.line() << ", " << in.column() << ": " << ident << " org " << expr;
        }
    };

    struct section_naked
        : seq< section, wsc, name > {};
    template<> struct action< section_naked >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // The last token is the section identifier
            std::string ident = data.popIdentifier();
            // Create new section and push it to current location's sections
            data.addNewSection(new lexer::Section(in.line(), in.column(), ident));
            LOG(LogLevel::DEBUG) << "SECTION at " << in.line() << ", " << in.column() << ": " << ident;
        }
    };

    // putting it all together
    struct section_decl
        : seq< sor< section_align_offset, section_align, section_org, section_naked >, wsc, must< body_open >, wsc, sec_body, wsc, must< body_close > > {};

}}}
