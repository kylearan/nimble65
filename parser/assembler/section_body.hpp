#pragma once

#include <string>
#include <iostream>

#include <pegtl.hh>

#include "common.hpp"
#include "expression.hpp"
#include "varconst.hpp"
#include "section.hpp"
#include "instruction.hpp"
#include "constraint.hpp"

namespace nimble65 { namespace parser { namespace assembler
{
    using namespace pegtl;

    //////////////////////////////////////////////////////////////////////
    // Tokens inside a section body: SAMEPAGE, CROSSPAGE, instructions
    //////////////////////////////////////////////////////////////////////

    template<> struct action< samepage >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // Note type and start of constraint
            auto s = data.getCurrentSection();
            lexer::Constraint c{lexer::ConstraintType::samepage, s->getSizeExpr(), ""};
            s->constraints.push_back(c);
            LOG(LogLevel::DEBUG) << "SAMEPAGE start at " << in.line() << ", " << in.column() << ": " << c.fromExpr;
        }
    };

    template<> struct action< crosspage >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // Note type and start of constraint
            auto s = data.getCurrentSection();
            lexer::Constraint c{lexer::ConstraintType::crosspage, s->getSizeExpr(), ""};
            s->constraints.push_back(c);
            LOG(LogLevel::DEBUG) << "CROSSPAGE start at " << in.line() << ", " << in.column() << ": " << c.fromExpr;
        }
    };

    // Global label declaration
    struct global_label_decl
        : seq< name, one< ':' > > {};
    template<> struct action< global_label_decl >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // Create new symbol for global label
            std::string ident = data.popIdentifier();
            auto section = data.getCurrentSection();
            std::string labelExpr = section->label + "+" + section->getSizeExpr();
            data.addNewSymbol(in.line(), in.column(), ident, labelExpr);
            LOG(LogLevel::DEBUG) << "LABEL at " << in.line() << ", " << in.column() << ": " << ident << " = " << labelExpr;
        }
    };

    // Local label declaration
    struct local_label_decl
        : seq< one< '.' >, name, one< ':' > > {};
    template<> struct action< local_label_decl >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // Create new symbol for local label
            auto section = data.getCurrentSection();
            std::string ident = std::string("__" + section->label + "_" + data.popIdentifier());
            std::string labelExpr = section->label + "+" + section->getSizeExpr();
            data.addNewSymbol(in.line(), in.column(), ident, labelExpr);
            LOG(LogLevel::DEBUG) << "LOCAL_LABEL at " << in.line() << ", " << in.column() << ": " << ident << " = " << labelExpr;
        }
    };

    // Tokens that can appear inside a section body, includimg inside a
    // samepage or crosspage body
    struct sec_token
        : seq< wsc, sor< instruction, varconst_decl, local_label_decl, global_label_decl > > {};

    // samepage declaration
    struct samepage_decl
        : seq< samepage, wsc, must< body_open >, plus< sec_token >, wsc, must< body_close > > {};
    template<> struct action< samepage_decl >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // Note from size of already constructed constraint
            auto s = data.getCurrentSection();
            auto c = &(s->constraints.back());
            c->toExpr = s->getSizeExpr() + "-1";
            LOG(LogLevel::DEBUG) << "SAMEPAGE end at " << in.line() << ", " << in.column() << ": " << c->fromExpr << " to " << c->toExpr;
        }
    };

    // samepage declaration
    struct crosspage_decl
        : seq< crosspage, wsc, must< body_open >, plus< sec_token >, wsc, must< body_close > > {};
    template<> struct action< crosspage_decl >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // Note from size of already constructed constraint
            auto s = data.getCurrentSection();
            auto c = &(s->constraints.back());
            c->toExpr = s->getSizeExpr() + "-1";
            LOG(LogLevel::DEBUG) << "CROSSPAGE end at " << in.line() << ", " << in.column() << ": " << c->fromExpr << " to " << c->toExpr;
        }
    };

    // Putting it all together
    struct sec_body
        : star< wsc, sor< samepage_decl, crosspage_decl, sec_token > > {};
}}}
