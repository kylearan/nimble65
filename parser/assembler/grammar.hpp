#pragma once

#include <string>
#include <iostream>

#include <pegtl.hh>

#include "common.hpp"
#include "expression.hpp"
#include "varconst.hpp"
#include "location.hpp"
#include "section.hpp"


namespace nimble65 { namespace parser { namespace assembler
{
    using namespace pegtl;


    //////////////////////////////////////////////////////////////////////
    // Main grammar
    //////////////////////////////////////////////////////////////////////

    struct grammar
        : seq< plus< wsc, sor< location_decl, varconst_decl > >, wsc, must< eof > > {};
    template<> const std::string errors< eof >::error_message = "location, variable or constant declaration expected";

}}}
