#include <string>
#include <regex>

namespace nimble65 { namespace parser { namespace assembler
{
    std::string stripWhitespace(std::string in) {
        const std::regex whitespace("[[:space:]]");
        return regex_replace(in, whitespace, "");
    }

    std::string stripComments(std::string in) {
        const std::regex comment(";.*\n");
        return regex_replace(in, comment, "");
    }

    std::string strip(std::string in) {
        // Add newline in case the input ends with a comment not delimeted
        // by a newline, so the regex will find it. It will be removed
        // afterwards anyway.
        return stripWhitespace(stripComments(in.append("\n")));
    }

}}}
