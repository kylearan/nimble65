#pragma once

#include <string>
#include <regex>
#include <iostream>

#include <pegtl.hh>

#include "logger.hpp"
#include "common.hpp"
#include "lexer/token.hpp"
#include "lexer/expression.hpp"

namespace nimble65 { namespace parser { namespace assembler
{
    using namespace pegtl;

    //////////////////////////////////////////////////////////////////////
    // Expressions and numbers
    //////////////////////////////////////////////////////////////////////

    // Numbers in decimal, hex, and binary
    struct number_dec
        : seq< opt< one< '+', '-' > >, plus< digit > > {};

    struct hexdigit
        : ranges< '0', '9', 'a', 'f', 'A', 'F' > {};
    struct number_hex
        : seq< opt< one< '+', '-' > >, one< '$' >, must< plus< hexdigit > > > {};
    template<> const std::string errors< plus< hexdigit > >::error_message = "hexadecimal number expected";

    struct bindigit
        : sor< one< '0' >, one< '1' > > {};
    struct number_bin
        : seq< opt< one< '+', '-' > >, one< '%' >, must< plus< bindigit > > > {};
    template<> const std::string errors< plus< bindigit > >::error_message = "binary number expected";

    struct expr_identifier
        : seq< opt< one< '.' > >, name > {};

    struct number
        : seq< not_at< keyword >, sor< expr_identifier, number_hex, number_bin, number_dec > > {};

    // Expression
    struct expr;

    struct lohibyte
        : sor< one< '<' >, one< '>' > > {};

    struct factor
        : sor< seq< one< '(' >, wsc, expr, wsc, one< ')' >, wsc >, number > {};

    struct infix_two
        : sor< two< '<' >, two< '>' >, two< '&' >, two< '|' > > {};
    struct infix_one
        : one< '*', '/', '<', '>', '&', '|', '^', '%' > {};
    struct infix
        : sor< infix_two, infix_one > {};
    struct term
        : seq< factor, wsc, star< infix, wsc, factor > > {};

    struct expr
        : seq< opt< lohibyte >, term, wsc, star< sor< one< '+' >, one< '-' > >, wsc, opt< lohibyte >, wsc, term > > {};
    template<> const std::string errors< expr >::error_message = "expression expected";
    // Adds the expression to the current list of active tokens
    template<> struct action< expr >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // We remove whitespace and comments here so that subsequent
            // grammar rules don't have to deal with it.
            std::string exprStripped = strip(in.string());
            LOG(LogLevel::DEBUG) << "EXPR at " << in.line() << ", " << in.column() << ": " << exprStripped;
            // If there's already a token active and this token starts earlier, then
            // remove the last token as it was detected too early.
            while (data.activeTokens.size() > 0 &&
                (data.activeTokens.back()->line > (int)in.line() ||
                (data.activeTokens.back()->line == (int)in.line() && data.activeTokens.back()->column > (int)in.column()))) {
                    LOG(LogLevel::DEBUG) << "...remove last active token which has been later at " << data.activeTokens.back()->line << ", " << data.activeTokens.back()->column;
                    data.popActiveToken();
                }

            // Replace any local labels in the expression with expanded labels
            if (data.sectionsExist()) {
                auto section = data.getCurrentSection();
                std::regex findRegex("\\.([a-zA-Z0-9_]+)");
                std::string replaceString = "__" + section->label + "_$1";
                exprStripped = std::regex_replace(exprStripped, findRegex, replaceString);
            }
            // Add new expression token to active tokens
            auto expr = new lexer::Expression(in.line(), in.column(), exprStripped);
            data.activeTokens.push_back(std::unique_ptr<lexer::Expression>(expr));
            data.removeActiveDuplicates();
        }
    };

}}}
