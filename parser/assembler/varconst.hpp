#pragma once

#include <string>
#include <iostream>
#include <cassert>

#include <pegtl.hh>

#include "primitives.hpp"
#include "common.hpp"
#include "expression.hpp"
#include "lexer/expression.hpp"
#include "lexer/identifier.hpp"
#include "lexer/symbol.hpp"


namespace nimble65 { namespace parser { namespace assembler
{
    using namespace pegtl;

    //////////////////////////////////////////////////////////////////////
    // identifier = expr
    //////////////////////////////////////////////////////////////////////

    struct const_decl
        : seq< name, ws, one< '=' >, ws, must< expr > > {};
    // Adds the expression to the pool of unresolved expressions
    template<> struct action< const_decl >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // Remove the last two active tokens which should be an expression
            // and an identifier
            std::string expr = data.popExpression();
            std::string ident = data.popIdentifier();
            LOG(LogLevel::DEBUG) << "CONST_DECL at " << in.line() << ", " << in.column() << ": " << ident << " = " << expr;
            // add symbol to list of unresolved symbols.
            // Defer check for duplicate identifiers to resolve phase.
            data.addNewSymbol(in.line(), in.column(), ident, expr);
        }
    };


    //////////////////////////////////////////////////////////////////////
    // varstart expr
    //////////////////////////////////////////////////////////////////////

    struct varstart_decl
        : seq< varstart, ws, must< expr > > {};

    template<> struct action< varstart_decl >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // Get and set varstart expression
            std::string expr = data.popExpression();
            LOG(LogLevel::DEBUG) << "VARSTART at " << in.line() << ", " << in.column() << ": " << expr;
            data.curVarstart = expr;
        }
    };


    //////////////////////////////////////////////////////////////////////
    // var identifier([expr])?
    //////////////////////////////////////////////////////////////////////

    // var identifier[expr]
    struct range_open
        : one< '[' > {};
    template<> const std::string errors< range_open >::error_message = "'[' expected";
    struct range_close
        : one< ']' > {};
    template<> const std::string errors< range_close >::error_message = "']' expected";

    struct var_range_decl
        : seq< name, range_open, wsc, must< expr >, wsc, must< range_close > > {};
    template<> struct action< var_range_decl >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // Get var identifier and range expression
            std::string expr = data.popExpression();
            std::string ident = data.popIdentifier();
            LOG(LogLevel::DEBUG) << "VAR[RANGE] at " << in.line() << ", " << in.column() << ": " << ident << "[" << expr << "]";
            // Add var as symbol to unrsolved symbols using the current
            // varstart expression and concatenate expr to varstart
            data.addNewSymbol(in.line(), in.column(), ident, data.curVarstart);
            data.curVarstart.append("+(" + expr + ")");
        }
    };

    // var identifier (without range)
    struct var_single_decl
        : seq< name > {};
    template<> struct action< var_single_decl >
    {
        static void apply( const input & in, ParsingData & data )
        {
            std::string ident = data.popIdentifier();
            LOG(LogLevel::DEBUG) << "VAR at " << in.line() << ", " << in.column() << ": " << ident;
            // Add var as symbol to unrsolved symbols using the current
            // varstart expression and concatenate "+1" to varstart
            data.addNewSymbol(in.line(), in.column(), ident, data.curVarstart);
            data.curVarstart.append("+1");
        }
    };

    struct var_decl
        : sor< var_range_decl, var_single_decl > {};
    template<> const std::string errors< var_decl >::error_message = "variable identifier expecred";

    struct var_list_decl
        : seq< var, wsc, must< var_decl >, wsc, star< one< ',' >, wsc, must< var_decl >, wsc > > {};


    //////////////////////////////////////////////////////////////////////
    // Line that can appear anywhere with one declaration of:
    // - constant
    // - varstart
    // - var
    //////////////////////////////////////////////////////////////////////

    // var_range_decl has to come before var_decl so it gets consumed first!
    struct varconst_decl
        : sor< varstart_decl, var_list_decl, const_decl > {};

}}}
