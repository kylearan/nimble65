#pragma once

#include <string>
#include <iostream>

#include <pegtl.hh>

#include "common.hpp"
#include "expression.hpp"
#include "varconst.hpp"
#include "section.hpp"

namespace nimble65 { namespace parser { namespace assembler
{
    using namespace pegtl;

    //////////////////////////////////////////////////////////////////////
    // location EXPR (to EXPR)? { LOCATION_BODY }
    //////////////////////////////////////////////////////////////////////

    struct loc_body
        : star< wsc, sor< section_decl, varconst_decl > > {};

    // "to" inserts a dummy token to protect the first address. Two expressions
    // in a row are not allowed and will be removed by the expression rules.
    template<> struct action< to >
    {
        static void apply( const input & in, ParsingData & data )
        {
            auto i = new lexer::Identifier(in.line(), in.column(), "to");
            data.activeTokens.push_back(std::unique_ptr<lexer::Identifier>(i));
        }
    };

    // Location header without end address
    struct loc_from
        : seq< location, ws, must< expr > > {};
    template<> struct action< loc_from >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // Last token is the start address expression for the new location
            std::string expr = data.popExpression();
            auto l = new lexer::Location(in.line(), in.column(), expr);
            data.locationDefs.push_back(std::unique_ptr<lexer::Location>(l));
            LOG(LogLevel::DEBUG) << "LOCATION at " << in.line() << ", " << in.column() << " at " << expr;
        }
    };

    // Location header with end address
    struct loc_from_to
        : seq< location, ws, must< expr >, ws, to, ws, must< expr > > {};

    template<> struct action< loc_from_to >
    {
        static void apply( const input & in, ParsingData & data )
        {
            // Last tokens are start and end address expressions for the new location
            std::string expr2 = data.popExpression();
            data.popIdentifier();   // Dummy "to" token to protect second expression
            std::string expr1 = data.popExpression();
            auto l = new lexer::Location(in.line(), in.column(), expr1, expr2);
            data.locationDefs.push_back(std::unique_ptr<lexer::Location>(l));
            LOG(LogLevel::DEBUG) << "LOCATION_TO at " << in.line() << ", " << in.column() << " from " << expr1 << " to " << expr2;
        }
    };

    // Putting it all together
    struct location_decl
        : seq< sor< loc_from_to, loc_from >, wsc, must< body_open >, wsc, loc_body, wsc, must< body_close > > {};

}}}
