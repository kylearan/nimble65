#pragma once

#include <string>
#include <iostream>

#include <pegtl.hh>

#include "primitives.hpp"
#include "logger.hpp"
#include "lexer/token.hpp"
#include "lexer/identifier.hpp"
#include "lexer/symbol.hpp"
#include "parsingdata.hpp"
#include "keywords.hpp"


namespace nimble65 { namespace parser { namespace assembler
{
    using namespace pegtl;

    //////////////////////////////////////////////////////////////////////
    // Utility functions
    //////////////////////////////////////////////////////////////////////
    // Removes spaces, tabs, and newlines
    std::string stripWhitespace(std::string in);
    // Removes comments
    std::string stripComments(std::string in);
    // Removes comments and whitespace
    std::string strip(std::string in);


    //////////////////////////////////////////////////////////////////////
    // Common building blocks
    //////////////////////////////////////////////////////////////////////

    // Basic struct for parsing error reporting
    template< typename Rule >
    struct errors
        : public normal< Rule >
    {
        static const std::string error_message;

        template< typename Input, typename... States >
        static void raise( const Input& in, States&&... )
        {
            throw parse_error( error_message, in );
        }
    };

    // Base struct for actions
    template< typename Rule >
    struct action
          : nothing< Rule > {};

    // Body open and close
    struct body_open
        : pegtl_string_t("{") {};
    template<> const std::string errors< body_open >::error_message = "'{' expected";
    struct body_close
        : pegtl_string_t("}") {};
    template<> const std::string errors< body_close >::error_message = "'}' expected";

    // Comments
    struct comment
        : if_must< comment_delimeter, until< eolf > > {};
    template<> const std::string errors< until< eolf > >::error_message = "end of line expected";
    // Whitespace including comments potentially spanning multiple lines
    struct wsc
        : star< sor< space, comment > > {};

    // Name is an identifier that is not a keyword
    struct name
        : seq< not_at< keyword >, identifier > {};
    // Action to store all names for later processing
    template<> struct action< name >
    {
        static void apply( const input & in, ParsingData & data )
        {
            LOG(LogLevel::DEBUG) << "NAME at " << in.line() << ", " << in.column() << ": " << in.string();
            auto i = new lexer::Identifier(in.line(), in.column(), in.string());
            data.activeTokens.push_back(std::unique_ptr<lexer::Identifier>(i));
            data.removeActiveDuplicates();
        }
    };

}}}
