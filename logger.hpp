#pragma once

#include <iostream>
#include <sstream>

enum class LogLevel {
    DEBUG,
    VERBOSE,
    INFO,
    ERR,
    SILENT
};

struct LogConfig {
    bool headers = false;
    LogLevel level = LogLevel::INFO;
};

extern LogConfig logCfg;

class LOG {
public:
    LOG() {}
    LOG(LogLevel lvl) {
        msgLevel = lvl;
        if(logCfg.headers) {
            operator << ("[" + getLabel(lvl) + "] ");
        }
    }
    ~LOG() {
        if(opened) {
            std::cout << std::endl;
        }
        opened = false;
    }
    template<class T>
    LOG &operator<<(const T &msg) {
        if(msgLevel >= logCfg.level) {
            std::cout << msg;
            opened = true;
        }
        return *this;
    }
private:
    bool opened = false;
    LogLevel msgLevel = LogLevel::DEBUG;
    inline std::string getLabel(LogLevel lvl) {
        std::string label;
        switch(lvl) {
            case LogLevel::DEBUG:   label = " DEBUG "; break;
            case LogLevel::VERBOSE: label = "VERBOSE"; break;
            case LogLevel::INFO:    label = " INFO  "; break;
            case LogLevel::ERR:     label = " ERROR "; break;
        }
        return label;
    }
};
