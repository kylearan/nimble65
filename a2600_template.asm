; TIA write only
varstart $00
var VSYNC   ; ......1.  vertical sync set-clear
var VBLANK  ; 11....1.  vertical blank set-clear
var WSYNC   ; <strobe>  wait for leading edge of horizontal blank
var RSYNC   ; <strobe>  reset horizontal sync counter
var NUSIZ0  ; ..111111  number-size player-missile 0
var NUSIZ1  ; ..111111  number-size player-missile 1
var COLUP0  ; 1111111.  color-lum player 0 and missile 0
var COLUP1  ; 1111111.  color-lum player 1 and missile 1
var COLUPF  ; 1111111.  color-lum playfield and ball
var COLUBK  ; 1111111.  color-lum background
var CTRLPF  ; ..11.111  control playfield ball size & collisions
var REFP0   ; ....1...  reflect player 0
var REFP1   ; ....1...  reflect player 1
var PF0     ; 1111....  playfield register byte 0
var PF1     ; 11111111  playfield register byte 1
var PF2     ; 11111111  playfield register byte 2
var RESP0   ; <strobe>  reset player 0
var RESP1   ; <strobe>  reset player 1
var RESM0   ; <strobe>  reset missile 0
var RESM1   ; <strobe>  reset missile 1
var RESBL   ; <strobe>  reset ball
var AUDC0   ; ....1111  audio control 0
var AUDC1   ; ....1111  audio control 1
var AUDF0   ; ...11111  audio frequency 0
var AUDF1   ; ...11111  audio frequency 1
var AUDV0   ; ....1111  audio volume 0
var AUDV1   ; ....1111  audio volume 1
var GRP0    ; 11111111  graphics player 0
var GRP1    ; 11111111  graphics player 1
var ENAM0   ; ......1.  graphics (enable) missile 0
var ENAM1   ; ......1.  graphics (enable) missile 1
var ENABL   ; ......1.  graphics (enable) ball
var HMP0    ; 1111....  horizontal motion player 0
var HMP1    ; 1111....  horizontal motion player 1
var HMM0    ; 1111....  horizontal motion missile 0
var HMM1    ; 1111....  horizontal motion missile 1
var HMBL    ; 1111....  horizontal motion ball
var VDELP0  ; .......1  vertical delay player 0
var VDELP1  ; .......1  vertical delay player 1
var VDELBL  ; .......1  vertical delay ball
var RESMP0  ; ......1.  reset missile 0 to player 0
var RESMP1  ; ......1.  reset missile 1 to player 1
var HMOVE   ; <strobe>  apply horizontal motion
var HMCLR   ; <strobe>  clear horizontal motion registers
var CXCLR   ; <strobe>  clear collision latches

; TIA read only
varstart $30
var CXM0P   ; 11......  read collision M0-P1, M0-P0 (Bit 7,6)
var CXM1P   ; 11......  read collision M1-P0, M1-P1
var CXP0FB  ; 11......  read collision P0-PF, P0-BL
var CXP1FB  ; 11......  read collision P1-PF, P1-BL
var CXM0FB  ; 11......  read collision M0-PF, M0-BL
var CXM1FB  ; 11......  read collision M1-PF, M1-BL
var CXBLPF  ; 1.......  read collision BL-PF, unused
var CXPPMM  ; 11......  read collision P0-P1, M0-M1
var INPT0   ; 1.......  read pot port
var INPT1   ; 1.......  read pot port
var INPT2   ; 1.......  read pot port
var INPT3   ; 1.......  read pot port
var INPT4   ; 1.......  read input
var INPT5   ; 1.......  read input

; PIA
varstart $280
var SWCHA   ; 11111111  Port A; input or output  (read or write)
var SWACNT  ; 11111111  Port A DDR, 0= input, 1=output
var SWCHB   ; 11111111  Port B; console switches (read only)
var SWBCNT  ; 11111111  Port B DDR (hardwired as input)
var INTIM   ; 11111111  Timer output (read only)
var INSTAT  ; 11......  Timer Status (read only, undocumented)

varstart $294
var TIM1T   ; 11111111  set 1 clock interval (838 nsec/interval)
var TIM8T   ; 11111111  set 8 clock interval (6.7 usec/interval)
var TIM64T  ; 11111111  set 64 clock interval (53.6 usec/interval)
var T1024T  ; 11111111  set 1024 clock interval (858.2 usec/interval)

; Start of RAM
varstart $80



TIM_OVERSCAN        = 50    ; TIM64T,  3200 cycles = ~ 42 scanlines
TIM_VBLANK          = 61    ; TIM64T,  3904 cycles = ~ 51 scanlines
TIM_KERNEL          = 17    ; T1024T, 17408 cycles = ~229 scanlines

location $1000 to $1fff {

    section start org $1000 {
        ; Clear zeropage
        cld
        ldx #0
        txa
    .clearLoop:
        dex
        tsx
        pha
        bne .clearLoop

    MainLoop:

        ; Overscan
        sta WSYNC
        lda #2
        sta VBLANK
        lda #TIM_OVERSCAN
        sta TIM64T

    .overscanEnd:
        lda INTIM
        bne .overscanEnd

        ; VBlank
         lda #%1110
    .vsyncLoop:
        sta WSYNC
        sta VSYNC
        lsr
        bne .vsyncLoop
        lda #2
        sta VBLANK
        lda #TIM_VBLANK
        sta TIM64T
   
    .waitForVBlank:
        lda INTIM
        bne .waitForVBlank
        sta WSYNC
        sta VBLANK

        ; Kernel
        lda #TIM_KERNEL
        sta T1024T

        ldx #200
    .loop:
        sta WSYNC
        stx COLUBK
        dex
        cpx #255
        bne .loop

    .waitForKernel:
        lda INTIM
        bne .waitForKernel

        jmp MainLoop
    }

    section vectors org $1ffc {
        dc.w start
        dc.w start
    }
}