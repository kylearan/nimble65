#pragma once

#include <utility>
#include <string>
#include <sstream>

namespace nimble65 { namespace lexer
{
    enum class TokenType {token, identifier, expression, symbol};

    //////////////////////////////////////////////////////////////////////
    // Base class for all lexer tokens, which can be expressions,
    // numbers, instructions, variable declarations etc.
    //////////////////////////////////////////////////////////////////////
    class Token {
    public:
        Token(int line, int column, TokenType type) :
            line(line),
            column(column),
            type(type)
            {}

        const int line;
        const int column;
        const TokenType type;

        bool isSameLocation(int line, int column) {
            return (this->line == line && this->column == column);
        }

        std::string toString() const {
            std::ostringstream oss;
            oss << "Token at " << line << ", " << column;
            return oss.str();
        }
    };
}}
