#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>
#include <cassert>

#include "instruction.hpp"

namespace nimble65 { namespace lexer {

class InstrImp : public Instruction
{
    public:
    InstrImp(const int line, const int column, const int pos, std::string instrString) :
        Instruction(line, column, pos, instrString, true)
        {};

    int size() override { return 1; }
    std::string sizeExpr() override { assert(false); return ""; }

    std::vector<unsigned char> generateBinary(mu::ParserInt & exprParser) override;
    
    protected:
        std::map<std::string, unsigned char> instrDefs = {
            {"asl", 0x0a},
            {"brk", 0x00},
            {"clc", 0x18},
            {"cld", 0xd8},
            {"cli", 0x58},
            {"clv", 0xb8},
            {"dex", 0xca},
            {"dey", 0x88},
            {"inx", 0xe8},
            {"iny", 0xc8},
            {"lsr", 0x4a},
            {"nop", 0xea},
            {"pha", 0x48},
            {"pla", 0x68},
            {"plp", 0x28},
            {"php", 0x08},
            {"rol", 0x2a},
            {"ror", 0x6a},
            {"rti", 0x40},
            {"rts", 0x60},
            {"sec", 0x38},
            {"sei", 0x78},
            {"tax", 0xaa},
            {"tay", 0xa8},
            {"tsx", 0xba},
            {"txa", 0x8a},
            {"txs", 0x9a},
            {"tya", 0x98},

            {"jam", 0x02}
        };
};

}}
