#include "instrrel.hpp"
#include "exceptions.hpp"

#include "muParserInt.h"
#include "logger.hpp"

namespace nimble65 { namespace lexer {

std::vector<unsigned char> InstrRel::generateBinary(mu::ParserInt & exprParser) {
    std::vector<unsigned char> binary;
    int value = 0;

    assert(instrDefs.find(instrString) != instrDefs.end());    
    exprParser.SetExpr(addrExpr);
    try {
        value = (int)exprParser.Eval();
    } catch (mu::ParserInt::exception_type) {
        throw link_error(line, column, "Unable to resolve expression: " + addrExpr);
    }
    if (value < -128 || value > 127) {
        throw link_error(line, column, "Branch target out of range: " + addrExpr + " = $" + link_error::toHex(value));
    }
    binary.push_back(instrDefs[instrString]);
    binary.push_back(unsigned char(value));    
    return binary;
}

}}
