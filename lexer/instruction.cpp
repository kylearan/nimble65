#include "instrimm.hpp"
#include "exceptions.hpp"

#include "muParserInt.h"
#include "logger.hpp"

namespace nimble65 { namespace lexer {

unsigned char Instruction::checkAndNormalizeByte(int value, std::string exprString) {
    if (value < -128 || value > 255) {
        throw link_error(line, column, "Byte expression out of range: " + exprString + " = $" + link_error::toHex(value));
    }
    // Cast negative value to unsigned byte
    if (value < 0) {
        value = 256 + value;
    }
    return unsigned char(value);
}

unsigned int Instruction::checkAndNormalizeWord(int value, std::string exprString) {
    if (value < -32768 || value > 65535) {
        throw link_error(line, column, "Word expression out of range: " + exprString + " = $" + link_error::toHex(value));
    }
    // Cast negative value to unsigned byte
    if (value < 0) {
        value = 65536 + value;
    }
    return unsigned int(value);
}

void Instruction::emitWord(int word, std::vector<unsigned char> & binary) {
    assert(word >= 0 && word <= 65535);
    binary.push_back(unsigned char(word%256));
    binary.push_back(unsigned char(word/256));    
}

std::vector<unsigned char> Instruction::generateByteOperandBinary(
    const std::string & byteExpr,
    mu::ParserInt & exprParser,
    std::map<std::string, unsigned char> & instrDefs
    ) {
        std::vector<unsigned char> binary;
        int value = 0;
    
        assert(instrDefs.find(instrString) != instrDefs.end());    
        exprParser.SetExpr(byteExpr);
        try {
            value = (int)exprParser.Eval();
        } catch (mu::ParserInt::exception_type) {
            throw link_error(line, column, "XUnable to resolve expression: " + byteExpr);
        }
        unsigned char byte = checkAndNormalizeByte(value, byteExpr);
        binary.push_back(instrDefs[instrString]);
        binary.push_back(byte);    
        return binary;            
    }

}}
