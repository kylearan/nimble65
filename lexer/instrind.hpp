#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>
#include <cassert>

#include "instruction.hpp"

namespace nimble65 { namespace lexer {

class InstrInd : public Instruction
{
    public:
    InstrInd(const int line, const int column, const int pos, std::string instrString,
        std::string addrExpr) :
        Instruction(line, column, pos, instrString, true),
        addrExpr(addrExpr)
        {};

    const std::string addrExpr;

    int size() override { return 3; }
    std::string sizeExpr() override { assert(false); return ""; }

    std::vector<unsigned char> generateBinary(mu::ParserInt & exprParser) override;
    
    protected:
        std::map<std::string, unsigned char> instrDefs = {
            {"jmp", 0x6c},

            {"jam", 0xd2},

        };
};

}}
