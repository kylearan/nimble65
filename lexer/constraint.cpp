#include <string>

#include "constraint.hpp"
#include "muParserInt.h"

namespace nimble65 { namespace lexer {

int Constraint::getFrom(mu::ParserInt & exprParser) {
    if (from == -1) {
        from = evalExpression(exprParser, fromExpr);
    }
    return from;
}

int Constraint::getTo(mu::ParserInt & exprParser) {
    if (to == -1) {
        to = evalExpression(exprParser, toExpr);
    }
    return to;
}


int Constraint::evalExpression(mu::ParserInt exprParser, std::string exprString) {
    int result = 0;
    exprParser.SetExpr(exprString);
    try {
        result = (int)exprParser.Eval();
    } catch (mu::ParserInt::exception_type) {
        assert(false);
    }
    return result;
}

}}
