#include <vector>

#include "logger.hpp"
#include "location.hpp"
#include "parsingdata.hpp"
#include "exceptions.hpp"
#include "constraint.hpp"


namespace nimble65 { namespace lexer {

int Location::nextNumber = 0;

Location::Location(const int line, const int column, std::string startAddrExpr) :
    line(line),
    column(column),
    number(nextNumber++),
    startAddrExpr(startAddrExpr),
    hasFixedSize(false),
    endAddrExpr("")
{}

Location::Location(const int line, const int column, std::string startAddrExpr, std::string endAddrExpr) :
    line(line),
    column(column),
    number(nextNumber++),
    startAddrExpr(startAddrExpr),
    hasFixedSize(true),
    endAddrExpr(endAddrExpr)
{}


void Location::initMemory(ParsingData *data) {
    // Start address
    startAddress = resolveExpression(data->exprParser, startAddrExpr,
        std::string("Unable to resolve start address of location " + std::to_string(number) + ": " + startAddrExpr));
    if (startAddress < 0) {
        throw link_error(line, column,
            std::string("Invalid start address of location " + std::to_string(number) + ": " + startAddrExpr));
    }
    // End address
    if (hasFixedSize) {
        endAddress = resolveExpression(data->exprParser, endAddrExpr,
            std::string("Unable to resolve end address of location " + std::to_string(number) + ": " + endAddrExpr));
    } else {
        endAddress = MAX_ADDRESS;
    }
    if (endAddress < 0 || endAddress < startAddress) {
        throw link_error(line, column,
            std::string("Invalid end address of location " + std::to_string(number) + ": " + endAddrExpr));
    }
    // Init free memory.
    freeMem.push_back({startAddress, endAddress - startAddress + 1});
    LOG(LogLevel::DEBUG) << "Init memory for location " << number << " at $" << std::hex << startAddress << " with size $" << (endAddress - startAddress) << std::dec;
}


bool Location::fitsIntoFreeMem(const int startAddress, const int size) {
    for (auto const& chunk : freeMem) {
        if (chunk.start <= startAddress &&
            chunk.size - (startAddress - chunk.start) >= size) {
            return true;
        }
    }
    return false;
}


int Location::getFree(const int address) {
    for (auto const& chunk : freeMem) {
        if (chunk.start <= address && chunk.start + chunk.size > address) {
            int result = chunk.size - (address - chunk.start);
            assert(result >= 0);
            return result;
        }
    }
    return 0;
}


int Location::getFreeBefore(const int address) {
    for (auto const& chunk : freeMem) {
        if (chunk.start <= address && chunk.start + chunk.size > address) {
            int result = address - chunk.start;
            assert(result >= 0);
            return result;
        }
    }
    return 0;
}


void Location::allocateMem(const int startAddress, const int size) {
    // Find chunk to fragment
    int insertAt = -1;
    for (size_t i = 0; i < freeMem.size(); ++i) {
        if (freeMem[i].start <= startAddress
            && startAddress < freeMem[i].start + freeMem[i].size) {
            insertAt = i;
            break;
        }
    }
    assert(insertAt != -1);

    // Catch all possible cases
    const int fmStart = freeMem[insertAt].start;
    const int fmSize = freeMem[insertAt].size;    
    if (startAddress == fmStart) {
        if (size == fmSize) {
            // Chunk exactly fits into memory block: Simply remove it
            freeMem.erase(freeMem.begin() + insertAt);
        } else {
            // Chunk starts at memory block but doesn't fill it completely:
            // Increase start address and reduce size
            freeMem[insertAt].start += size;
            freeMem[insertAt].size -= size;
        }
    } else {
        if (fmSize - (startAddress - fmStart) == size) {
            // Chunk starts in the middle of memory, but fills it until end:
            // Reduce size
            freeMem[insertAt].size -= size;
        } else {
            // Chunk is in the middle of memory and fragments it into two new chunks:
            // Adapt existing chunk's size and insert a new chunk of free mem after it
            freeMem[insertAt].size = startAddress - fmStart;
            memChunk m{startAddress + size, fmStart + fmSize - (startAddress + size)};
            freeMem.insert(freeMem.begin() + insertAt + 1, m);
        }
    }
}


void Location::printFreeMemory() {
    LOG(LogLevel::VERBOSE) << "Free memory of location " << number << ":";
    for (auto const& chunk : freeMem) {
        std::stringstream ss;
        ss << "$" << std::hex << chunk.start << " - $" << chunk.start + chunk.size - 1;
        LOG(LogLevel::VERBOSE) << "    " << ss.str();
    }
}
    


void Location::linkNonOrgSections(ParsingData *data) {
    LOG(LogLevel::VERBOSE) << "Linking sections with variable addresses...";
    while (sectionDefs.size() > 0) {
        // Get largest section left and its data
        int sectionSize = 0;
        int si = getLargestSection(data->exprParser, sectionSize);
        Section *sec = sectionDefs[si].get();
        if (sectionSize == 0) {
            LOG(LogLevel::INFO) << "Ignoring empty section " << sec->label;
            data->deleteSymbol(sec->label);
            sectionDefs.erase(sectionDefs.begin() + si);
        } else {
            LOG(LogLevel::VERBOSE) << "Linking section " << sec->label << ", size $" << std::hex << sectionSize << std::dec;
            std::vector<memChunk> candidates = createListOfCandidates(sectionSize);
            // Try to find the smalles memory chunk this section fits into with
            // the least amount of waste
            int sectionAddress = -1;
            int minWaste = MAX_ADDRESS;
            while (sectionAddress == -1 && candidates.size() > 0) {
                int iCandidate = getSmallestCandidateIndex(candidates);
                int curStart = candidates[iCandidate].start;
                int curSize = candidates[iCandidate].size;
                int candidateAddr = -1;
                int candidateWaste = -1;
                // Since constraint result are the same for mod 256, we only need to look
                // at the first and last 256 bytes of a mem chunk for potential start
                // addresses.
                // Start of memChunk
                int endAddr = std::min(curStart + 255, curStart + curSize - sectionSize);
                calcMinimumWaste(data, sec, curStart, endAddr, minWaste, sectionAddress);
                // End of memChunk only if it is not unlimited
                if (curStart + curSize - 1 < MAX_ADDRESS) {
                    int startAddr = std::max(curStart + curSize - sectionSize - 255, curStart);
                    calcMinimumWaste(data, sec, startAddr, curStart + curSize - sectionSize, minWaste, sectionAddress);
                }
                // If section doesn't fit into this memChunk: Erase this chunk
                // and continue searching
                if (sectionAddress == -1) {
                    candidates.erase(candidates.begin() + iCandidate);                
                }
            }
            // If no suitable memChunk can be found, throw error
            if (sectionAddress == -1) {
                throw link_error(sec->line, sec->column,
                    "Unable to find valid memory location of size $" + link_error::toHex(sectionSize) + " for section " + sec->label + "!");
            }
            // Allocate memory, set start address and create new symbol for it
            LOG(LogLevel::VERBOSE) << "    ...at address $" << link_error::toHex(sectionAddress) << " with waste ($" << getFreeBefore(sectionAddress) << ", $" << link_error::toHex(getFree(sectionAddress + sectionSize)) << ")";
            setSectionStartAddress(data, sec, sectionAddress, sectionSize, si);
        }
    }
    LOG(LogLevel::VERBOSE) << "Linking sections with variable addresses done.";
}


void Location::linkOrgSections(ParsingData *data) {
    LOG(LogLevel::VERBOSE) << "Linking sections with fixed addresses...";
    for (size_t si = 0; si < sectionDefs.size(); ) {
        Section *s = sectionDefs[si].get();
        if (s->hasOrg) {
            // Resolve start address and try to fit section into free memory
            const int totalSize = s->getTotalSize(data->exprParser);
            if (totalSize == 0) {
                LOG(LogLevel::INFO) << "Ignoring empty section " << s->label;
                data->deleteSymbol(s->label);
                sectionDefs.erase(sectionDefs.begin() + si);
            } else {
                LOG(LogLevel::DEBUG) << "Add section " << s->label << " at ORG " << s->orgExpr << ", size $" << std::hex << s->getTotalSize(data->exprParser) << std::dec;
                int startAddress = s->getOrg(data->exprParser);
                if (startAddress < getStartAddress() || startAddress > getEndAddress()) {
                    throw link_error(s->line, s->column,
                    std::string("Start address of ORG section " + s->label + " is outside its location's memory block!"));
                }
                if (fitsIntoFreeMem(startAddress, totalSize)) {
                    // Allocate memory, set start address and create new symbol for it
                    setSectionStartAddress(data, s, startAddress, totalSize, si);
                } else {
                    throw link_error(s->line, s->column,
                        std::string("ORG section " + s->label + " does not fit into memory! "
                        + "Needs " + std::to_string(totalSize) + " bytes, only has "
                        + std::to_string(getFree(startAddress))));
                }
            }
        } else {
            si++;
        }
    }
    LOG(LogLevel::VERBOSE) << "Linking sections with fixed addresses done.";
}


void Location::linkSections(ParsingData *data) {
    linkOrgSections(data);
    linkNonOrgSections(data);
    printFreeMemory();
}


int Location::getEarliestSectionStart() {
    int min = linkedSections[0]->getStartAddress();
    for (auto const& s : linkedSections) {
        if (s->getStartAddress() < min) {
            min = s->getStartAddress();
        }
    }
    return min;
}


int Location::getEarliestSectionIndex() {
    int min = linkedSections[0]->getStartAddress();
    int result = 0;
    for (size_t i = 0; i < linkedSections.size(); ++i) {
        if (linkedSections[i]->getStartAddress() < min) {
            min = linkedSections[i]->getStartAddress();
            result = (int)i;
        }
    }
    return result;    
}


int Location::getLargestSection(mu::ParserInt & exprParser, int & size) {
    size = -1;
    int curMaxIndex = -1;
    for (size_t i = 0; i < sectionDefs.size(); ++i) {
        int secSize = sectionDefs[i]->getTotalSize(exprParser);
        if (secSize > size) {
            size = secSize;
            curMaxIndex = i;
        }
    }
    return curMaxIndex;
}


void Location::calcMinimumWaste(ParsingData *data, Section *sec, int startAddr, int endAddr, int & minWaste, int & sectionAddress) {
    for (int addr = startAddr; addr <= endAddr; ++addr) {
        int curWaste = checkConstraintsAndCalcWaste(sec, data->exprParser, addr);
        // "<": If waste is the same, prefer smaller address
        if (curWaste != -1 && curWaste < minWaste) {
            minWaste = curWaste;
            sectionAddress = addr;
        }
    }
}


void Location::setSectionStartAddress(ParsingData *data, Section *sec, int addr, int size, int index) {
    allocateMem(addr, size);
    sec->setStartAddress(addr);
    data->addNewUniqueSymbol(sec->line, sec->column,
        std::string("__" + sec->label + "_address"), std::to_string(addr));
    // Move section to list of linked sections
    linkedSections.push_back(std::move(sectionDefs[index]));
    sectionDefs.erase(sectionDefs.begin() + index);        
}


std::vector<unsigned char> Location::generateBinary(mu::ParserInt & exprParser, unsigned char fillByte) {
    std::vector<unsigned char> binary;
    int curAddress = getStartAddress();
    if (linkedSections.size() > 0) {
        while (linkedSections.size() > 0) {
            // Find next section
            int secIndex = getEarliestSectionIndex();
            lexer::Section *sec = linkedSections[secIndex].get();
            LOG(LogLevel::DEBUG) << "    Processing section " << sec->label << " with start address $" << std::hex << sec->getStartAddress() << std::dec;
            assert(sec->getStartAddress() >= curAddress);
            // If there is a gap between last and this section, fill it
            if (sec->getStartAddress() > curAddress) {
                binary.insert(binary.end(), sec->getStartAddress() - curAddress, fillByte);
                curAddress = sec->getStartAddress();
            }
            // Add location to binary
            auto secBinary = sec->generateBinary(exprParser, fillByte);
            binary.insert(binary.end(), secBinary.begin(), secBinary.end());
            curAddress += secBinary.size();
            // Section successfully processed: Remove it
            linkedSections.erase(linkedSections.begin() + secIndex);
        }
    }
    // If there is a gap between last section and end of location, fill it
    if (getEndAddress() != MAX_ADDRESS && curAddress < getEndAddress() + 1) {
        binary.insert(binary.end(), getEndAddress() + 1 - curAddress, fillByte);
    }
    return binary;
}


int Location::resolveExpression(mu::ParserInt & exprParser, std::string exprString, std::string errorMsg) {
    int result = 0;
    exprParser.SetExpr(exprString);
    try {
        result = (int)exprParser.Eval();
    } catch (mu::ParserInt::exception_type) {
        throw link_error(line, column, errorMsg);
    }
    return result;
}


std::vector<Location::memChunk> Location::createListOfCandidates(int secSize) {
    std::vector<memChunk> list;
    for (auto chunk: freeMem) {
        if (chunk.size >= secSize) {
            list.push_back(chunk);
        }
    }
    return list;
}


int Location::getSmallestCandidateIndex(std::vector<memChunk> candidates) {
    int iSmallest = -1;
    int curMin = MAX_ADDRESS;
    for (size_t i = 0; i < candidates.size(); ++i) {
        if (candidates[i].size < curMin) {
            curMin = candidates[i].size;
            iSmallest = i;
        }
    }
    return iSmallest;
}


int Location::checkConstraintsAndCalcWaste(Section *sec, mu::ParserInt & exprParser, int addr) {
    // Check if alignment and offset constraints are satisfied
    if (sec->hasAlign) {
        if (sec->hasOffset) {
            int align = sec->getAlign(exprParser);
            int offset = sec->getOffset(exprParser);
            if (addr%align != offset) {
                return -1;
            }
        } else {
            int align = sec->getAlign(exprParser);
            if (addr%align != 0) {
                return -1;
            }
        }
    }
    // Check if all crosspage/samepage constraints are satisfied
    for (auto&& c : sec->constraints) {
        int from = addr + c.getFrom(exprParser);
        int to = addr + c.getTo(exprParser);
        switch (c.type) {
            case ConstraintType::crosspage:
                if (int(from/256) == int(to/256)) {
                    return -1;
                }
                break;
            case ConstraintType::samepage:
            if (int(from/256) != int(to/256)) {
                return -1;
            }
            break;
        default:
            assert(false);            
        }
    }
    // For waste, look at the bytes wasted at the start and the end.
    // Waste is minimum of both.
    int freeBefore = getFreeBefore(addr);
    int freeAfter = getFree(addr + sec->getTotalSize(exprParser));
    return std::min(freeBefore, freeAfter);
}

}}
