#include "instrind.hpp"
#include "exceptions.hpp"

#include "muParserInt.h"
#include "logger.hpp"

namespace nimble65 { namespace lexer {

std::vector<unsigned char> InstrInd::generateBinary(mu::ParserInt & exprParser) {
    std::vector<unsigned char> binary;
    int value = 0;
    
    assert(instrDefs.find(instrString) != instrDefs.end());  
    exprParser.SetExpr(addrExpr);
    try {
        value = (int)exprParser.Eval();
    } catch (mu::ParserInt::exception_type) {
        throw link_error(line, column, "Unable to resolve expression: " + addrExpr);
    }
    unsigned int word = checkAndNormalizeWord(value, addrExpr);
    binary.push_back(instrDefs[instrString]);
    emitWord(word, binary);
    return binary;
}

}}
