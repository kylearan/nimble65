#pragma once

#include <string>
#include <sstream>

#include "token.hpp"

namespace nimble65 { namespace lexer
{
    //////////////////////////////////////////////////////////////////////
    // Expression definition that can be evaluated.
    //////////////////////////////////////////////////////////////////////
    class Expression : public Token {
    public:
        Expression(int line, int column, std::string expr) :
            Token(line, column, TokenType::expression),
            expr(expr)
            {}

        std::string toString() const {
            std::ostringstream oss;
            oss << "Expression at " << line << ", " << column << ": " << expr;
            return oss.str();
        }

        const std::string expr;
    };
}}
