#pragma once

#include <string>
#include <sstream>

#include "token.hpp"

namespace nimble65 { namespace lexer
{
    //////////////////////////////////////////////////////////////////////
    // Identifier used in constant or var decls, labels etc.
    //////////////////////////////////////////////////////////////////////
    class Identifier : public Token {
    public:
        Identifier(int line, int column, std::string identifier) :
            Token(line, column, TokenType::identifier),
            identifier(identifier)
            {}

        const std::string identifier;

        std::string toString() const {
            std::ostringstream oss;
            oss << "Identifier at " << line << ", " << column << ": " << identifier;
            return oss.str();
        }
    };
}}
