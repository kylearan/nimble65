#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>
#include <cassert>

#include "instruction.hpp"

namespace nimble65 { namespace lexer {

//////////////////////////////////////////////////////////////////////
// A dc.b or dc.w data instruction
//////////////////////////////////////////////////////////////////////
class InstrData : public Instruction
{
    public:
        // width defines if its a dc.b (=1) or dc.w (=2) data instruction
        InstrData(const int line, const int column, const int pos, std::string instrString, std::string valueExpr, int width) :
            Instruction(line, column, pos, instrString, true),
            valueExpr(valueExpr),
            width(width)
            {};

        int size() override { return width; }
        std::string sizeExpr() override { assert(false); return ""; }

        std::vector<unsigned char> generateBinary(mu::ParserInt & exprParser) override;

        const std::string valueExpr;

    protected:
        const int width;
};

}}
