#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>
#include <cassert>

#include "instruction.hpp"
#include "instrabsbase.hpp"

namespace nimble65 { namespace lexer {

class InstrAbs : public InstrAbsBase
{
    public:
    InstrAbs(const int line, const int column, const int pos, std::string instrString,
        std::string addrExpr, bool forceWord) :
        InstrAbsBase(line, column, pos, instrString, addrExpr, forceWord)
        {};

        std::vector<unsigned char> InstrAbs::generateBinary(mu::ParserInt & exprParser) override {
            return generateAbsBinary(exprParser, instrWordDefs, instrZpDefs, "absolute");
        }
                            
    protected:
        std::map<std::string, unsigned char> instrWordDefs = {
            {"jsr", 0x20},
            {"bit", 0x2c},
            {"jmp", 0x4c},
            {"sty", 0x8c},
            {"ldy", 0xac},
            {"cpy", 0xcc},
            {"cpx", 0xec},
            {"ora", 0x0d},
            {"and", 0x2d},
            {"eor", 0x4d},
            {"adc", 0x6d},
            {"sta", 0x8d},
            {"lda", 0xad},
            {"cmp", 0xcd},
            {"sbc", 0xed},
            {"asl", 0x0e},
            {"rol", 0x2e},
            {"lsr", 0x4e},
            {"ror", 0x6e},
            {"stx", 0x8e},
            {"ldx", 0xae},
            {"dec", 0xce},
            {"inc", 0xee},

            {"nop", 0x0c},
            {"slo", 0x0f},
            {"rla", 0x2f},
            {"sre", 0x4f},
            {"rra", 0x6f},
            {"sax", 0x8f},
            {"lax", 0xaf},
            {"dcp", 0xcf},
            {"isc", 0xef},

            {"jam", 0x72},
        };

        std::map<std::string, unsigned char> instrZpDefs = {
            {"bit", 0x24},
            {"sty", 0x84},
            {"ldy", 0xa4},
            {"cpy", 0xc4},
            {"cpx", 0xe4},
            {"ora", 0x05},
            {"and", 0x25},
            {"eor", 0x45},
            {"adc", 0x65},
            {"sta", 0x85},
            {"lda", 0xa5},
            {"cmp", 0xc5},
            {"sbc", 0xe5},
            {"asl", 0x06},
            {"rol", 0x26},
            {"lsr", 0x46},
            {"ror", 0x66},
            {"stx", 0x86},
            {"ldx", 0xa6},
            {"dec", 0xc6},
            {"inc", 0xe6},

            {"nop", 0x04},
            {"slo", 0x07},
            {"rla", 0x27},
            {"sre", 0x47},
            {"rra", 0x67},
            {"sax", 0x87},
            {"lax", 0xa7},
            {"dcp", 0xc7},
            {"isc", 0xe7},

            {"jam", 0x22},
        };

};

}}
