#include "instrdata.hpp"
#include "exceptions.hpp"

#include "muParserInt.h"
#include "logger.hpp"

namespace nimble65 { namespace lexer {

std::vector<unsigned char> InstrData::generateBinary(mu::ParserInt & exprParser) {
    std::vector<unsigned char> binary;
    assert(width == 1 || width == 2);
    int value = 0;
    exprParser.SetExpr(valueExpr);
    try {
        value = (int)exprParser.Eval();
    } catch (mu::ParserInt::exception_type) {
        throw link_error(line, column, "Unable to resolve expression: " + valueExpr);
    }
    switch (width) {
        case 1: {
            unsigned char byte = checkAndNormalizeByte(value, valueExpr);
            binary.push_back(byte);    
        }
        break;
        case 2: {
            unsigned int word = checkAndNormalizeWord(value, valueExpr);
            // Little endian
            emitWord(word, binary);
        }
        break;
    }
    return binary;
}

}}
