#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>
#include <cassert>

#include "instruction.hpp"

namespace nimble65 { namespace lexer {

class InstrAbsBase : public Instruction
{
    public:
    InstrAbsBase(const int line, const int column, const int pos, std::string instrString,
        std::string addrExpr, bool forceWord) :
        Instruction(line, column, pos, instrString, forceWord),
        addrExpr(addrExpr)
        {};

    const std::string addrExpr;

    int size() override {
        return (hasFixedSize ? 3 : 1);
    }
    std::string sizeExpr() override {
        return (hasFixedSize ? std::string("0") : std::string("@(" + addrExpr + ")"));
    }

    // DIstinguishes between word and zp adressing and generates binary data
    // accordingly.
    std::vector<unsigned char> generateAbsBinary(
        mu::ParserInt & exprParser,
        std::map<std::string, unsigned char> & instrWordDefs,
        std::map<std::string, unsigned char> & instrZpDefs,
        std::string modeString 
        );

};

}}
