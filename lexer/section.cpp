#include "token.hpp"
#include "symbol.hpp"
#include "section.hpp"
#include "logger.hpp"
#include "instrabs.hpp"
#include "exceptions.hpp"


namespace nimble65 { namespace lexer
{

int Section::nextNumber = 0;

Section::Section(const int line, const int column, std::string label) :
    line(line),
    column(column),
    number(nextNumber++),
    label(label),
    hasAlign(false),
    alignExpr(""),
    hasOffset(false),
    offsetExpr(""),
    hasOrg(false),
    orgExpr("")
    {}

Section::Section(const int line, const int column, std::string label, std::string alignExpr, bool hasOffset, std::string offsetExpr) :
    line(line),
    column(column),
    number(nextNumber++),
    label(label),
    hasAlign(true),
    alignExpr(alignExpr),
    hasOffset(hasOffset),
    offsetExpr(offsetExpr),
    hasOrg(false),
    orgExpr("")
    {}

Section::Section(const int line, const int column, std::string label, std::string orgExpr) :
    line(line),
    column(column),
    number(nextNumber++),
    label(label),
    hasAlign(false),
    alignExpr(""),
    hasOffset(false),
    offsetExpr(""),
    hasOrg(true),
    orgExpr(orgExpr)
    {}


int Section::getTotalSize(mu::ParserInt & exprParser) {
    if (totalSize < 0) {
        totalSize = size;
        for (auto&& i : instructions) {
            if (!i->hasFixedSize) {
                lexer::InstrAbsBase *absInstr = dynamic_cast<lexer::InstrAbsBase *>(i.get());
                // Try to resolve expression. If successful, use size of
                // result. If not, assume size 2.
                int exprSize;
                int result = 0;
                exprParser.SetExpr(absInstr->addrExpr);
                try {
                    result = (int)exprParser.Eval();
                    // Illegal size errors will get caught later during
                    // binary data generation.
                    if (result >= 0 && result <= 255) {
                        exprSize = 1;
                    } else {
                        exprSize = 2;
                    }
                } catch (mu::ParserInt::exception_type) {
                    exprSize = 2;
                }
                totalSize += exprSize;
            }
        }
    }
    return totalSize;
}


int Section::getAlign(mu::ParserInt & exprParser) {
    if (align == -1) {
        align = resolveExpression(exprParser, alignExpr, "Unable to resolve align expression of section " + label + ": " + alignExpr);
    }
    return align;
}

int Section::getOffset(mu::ParserInt & exprParser) {
    if (offset == -1) {
        offset = resolveExpression(exprParser, offsetExpr, "Unable to resolve offset expression of section " + label + ": " + offsetExpr);
    }
    return offset;
}

int Section::getOrg(mu::ParserInt & exprParser) {
    if (org == -1) {
        org = resolveExpression(exprParser, orgExpr, "Unable to resolve org expression of section " + label + ": " + orgExpr);
    }
    return org;
}


std::vector<unsigned char> Section::generateBinary(mu::ParserInt & exprParser, unsigned char fillByte) {
    std::vector<unsigned char> binary;
    for (auto const& i : instructions) {
        std::vector<unsigned char> insBin = i->generateBinary(exprParser);
        binary.insert(binary.end(), insBin.begin(), insBin.end());
    }
    return binary;
}


int Section::resolveExpression(mu::ParserInt & exprParser, std::string exprString, std::string errorMsg) {
    int result = 0;
    exprParser.SetExpr(exprString);
    try {
        result = (int)exprParser.Eval();
    } catch (mu::ParserInt::exception_type) {
        throw link_error(line, column, errorMsg);
    }
    return result;
}

}}
