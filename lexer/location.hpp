#pragma once

#include <string>
#include <vector>
#include <memory>

#include "section.hpp"

namespace nimble65 {

// To avoid circular includes
class ParsingData;

namespace lexer {

class Location
{
    public:
        // Highest possible address, signifies "unlimited"
        const int MAX_ADDRESS = 999999999;

        const int line;
        const int column;
        const int number;
        const std::string startAddrExpr;
        const bool hasFixedSize;
        const std::string endAddrExpr;

        std::vector<std::unique_ptr<lexer::Section>> sectionDefs;
        std::vector<std::unique_ptr<lexer::Section>> linkedSections;

        struct memChunk {
            int start;
            int size;
        };
        std::vector<memChunk> freeMem;

        Location(const int line, const int column, std::string startAddrExpr);
        Location(const int line, const int column, std::string startAddrExpr, std::string endAddrExpr);

        // Resolve start and end address expressions and init memory
        void initMemory(ParsingData *data);
        // Check if a chunk of memory is free
        bool fitsIntoFreeMem(const int startAddress, const int size);
        // Returns the number of free bytes starting at address
        int getFree(const int address);
        // Returns the number of free bytes before an address
        int getFreeBefore(const int address);
        // Reserves a chunk of memory
        void allocateMem(const int startAddress, const int size);
        // Print out memory map
        void printFreeMemory();
    
        // Link sections with fixed address
        void linkOrgSections(ParsingData *data);
        // Link relocatable sections
        void linkNonOrgSections(ParsingData *data);
        // Link all sections (org and non-org)
        void linkSections(ParsingData *data);

        // Generate binary data out of this location. A from_to location will
        // have its fixed size.
        std::vector<unsigned char> generateBinary(mu::ParserInt & exprParser, unsigned char fillByte);

        // Gets start and end addresses. Only valid after linking.
        int getStartAddress() {
            assert(startAddress != -1);
            return startAddress;
        }
        int getEndAddress() {
            assert(endAddress != -1);
            return endAddress;
        }

    private:
        static int nextNumber;

        int startAddress = -1;
        int endAddress = -1;

        // Tries to resolve an expression with the expression parser and throws a
        // link_error exception in case of failure
        int resolveExpression(mu::ParserInt & exprParser, std::string exprString, std::string errorMsg);

        // Return the smallest start address of all sections
        int getEarliestSectionStart();
        // Return the index of the section with the smallest start address
        int getEarliestSectionIndex();
        // Return the index of the (not yet linked) section with the largest size.
        // Returns -1 if there are no sections.
        int getLargestSection(mu::ParserInt & exprParser, int & size);
        // Loops over a range of addresses and calcs minimum waste of bytes
        void calcMinimumWaste(ParsingData *data, Section *sec, int startAddr, int endAddr, int & minWaste, int & sectionAddress);
        // Allocate memory, create a symbol for it and move section to linkedSections
        void setSectionStartAddress(ParsingData *data, Section *sec, int addr, int size, int index);

        // Create a list of possible MemChunk candidates to link a section of
        // the given size into. Copies the MemChunk values from freeMem.
        std::vector<memChunk> createListOfCandidates(int secSize);
        // Get the index of the smallest memChunk
        int getSmallestCandidateIndex(std::vector<memChunk> candidates);
        // Return number of wasted bytes for a given section and address
        // observing all section constraints. If not all constraints are
        // satisfied, return -1.
        int checkConstraintsAndCalcWaste(Section *sec, mu::ParserInt & exprParser, int addr);
};

}}
