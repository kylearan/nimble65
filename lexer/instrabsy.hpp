#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>
#include <cassert>

#include "instruction.hpp"
#include "instrabsbase.hpp"

namespace nimble65 { namespace lexer {

class InstrAbsY : public InstrAbsBase
{
    public:
    InstrAbsY(const int line, const int column, const int pos, std::string instrString,
        std::string addrExpr, bool forceWord) :
        InstrAbsBase(line, column, pos, instrString, addrExpr, forceWord)
        {};

        std::vector<unsigned char> InstrAbsY::generateBinary(mu::ParserInt & exprParser) override {
            return generateAbsBinary(exprParser, instrWordDefs, instrZpDefs, "absolute,y");
        }
                
        protected:
        std::map<std::string, unsigned char> instrWordDefs = {
            {"ora", 0x19},
            {"and", 0x39},
            {"eor", 0x59},
            {"adc", 0x79},
            {"sta", 0x99},
            {"lda", 0xb9},
            {"cmp", 0xd9},
            {"sbc", 0xf9},
            {"ldx", 0xbe},

            {"slo", 0x1b},
            {"rla", 0x3b},
            {"sre", 0x5b},
            {"rra", 0x7b},
            {"lax", 0xbf},
            {"dcp", 0xdb},
            {"isc", 0xfb},

            {"sha", 0x9f},
            {"shx", 0x9e},
            {"tas", 0x9b},
            {"las", 0xbb},

            {"jam", 0xb2},
        };

        std::map<std::string, unsigned char> instrZpDefs = {
            {"stx", 0x96},
            {"ldx", 0xb6},

            {"sax", 0x97},
            {"lax", 0xb7},

            {"jam", 0x42}
        };
};

}}
