
#include <cassert>

#include "instruction.hpp"
#include "instrabsbase.hpp"
#include "exceptions.hpp"

#include "muParserInt.h"
#include "logger.hpp"

namespace nimble65 { namespace lexer {

std::vector<unsigned char> InstrAbsBase::generateAbsBinary(
    mu::ParserInt & exprParser,
    std::map<std::string, unsigned char> & instrWordDefs,
    std::map<std::string, unsigned char> & instrZpDefs,
    std::string modeString 
    ) {
    std::vector<unsigned char> binary;
    int value = 0;
    
    assert(instrWordDefs.find(instrString) != instrWordDefs.end() || instrZpDefs.find(instrString) != instrZpDefs.end());  
    exprParser.SetExpr(addrExpr);
    try {
        value = (int)exprParser.Eval();
    } catch (mu::ParserInt::exception_type) {
        throw link_error(line, column, "Unable to resolve expression: " + addrExpr);
    }
    unsigned int word = checkAndNormalizeWord(value, addrExpr);
    // Word-size or zero page?
    auto zpIt = instrZpDefs.find(instrString);
    if (word < 256 && !hasFixedSize && zpIt != instrZpDefs.end()) {
        binary.push_back(zpIt->second);
        binary.push_back(unsigned char(word));    
    } else {
        auto it = instrWordDefs.find(instrString);
        if (it == instrWordDefs.end()) {
            throw link_error(line, column, "This instruction has no " + modeString + " addressing mode: " + instrString);
        }
        binary.push_back(it->second);        
        emitWord(word, binary);
    }
    return binary;
}

}}
