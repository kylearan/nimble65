#pragma once

#include <string>

#include "muParserInt.h"

namespace nimble65 { namespace lexer {

enum class ConstraintType { samepage, crosspage };

class Constraint
{
    public:
    Constraint(ConstraintType type, std::string fromExpr, std::string toExpr) :
        type(type),
        fromExpr(fromExpr),
        toExpr(toExpr)
        {}

    ConstraintType type;
    std::string fromExpr;
    std::string toExpr;

    // Get from and to values. The respective expressions get evaluated
    // only at first call.
    int getFrom(mu::ParserInt & exprParser);
    int getTo(mu::ParserInt & exprParser);
    
    private:
    int from = -1;
    int to = -1;

    int evalExpression(mu::ParserInt exprParser, std::string exprString);
};

}}
