#pragma once

#include <string>
#include <algorithm>
#include <memory>
#include <vector>

#include "muParserInt.h"
#include "muParserBase.h"
#include "muParserError.h"


namespace nimble65 { namespace lexer {

class Instruction
{
    public:
    Instruction(const int line, const int column, const int pos,
        std::string instrString, bool hasFixedSize) :
        line(line),
        column(column),
        pos(pos),
        hasFixedSize(hasFixedSize)
        {
            std::transform(instrString.begin(), instrString.end(), instrString.begin(), ::tolower);
            this->instrString = instrString;
        };

    // Returns the size of this instruction in bytes
    virtual int size() = 0;
    // In case of absolute addressing, this expression defines the width of
    // the operand in bytes (1 or 2)
    virtual std::string sizeExpr() = 0;

    // Generates the binary data for this instruction. Needs an expression parser
    // object with defined symbols to resolve any expressions used in the instruction.
    virtual std::vector<unsigned char> generateBinary(mu::ParserInt & exprParser) = 0;

    const int line;
    const int column;
    const int pos;
    std::string instrString;
    const bool hasFixedSize;

    protected:
    // Checks an int for byte range (-128..255) and converts it to an unsigned char.
    // If it's out of range, a link_error is thrown.
    unsigned char checkAndNormalizeByte(int value, std::string exprString);
    // Checks an int for word range (-32768..65535) and converts it to an unsigned char.
    // If it's out of range, a link_error is thrown.
    unsigned int checkAndNormalizeWord(int value, std::string exprString);

    // Emit a word in little endian format to binary
    void emitWord(int word, std::vector<unsigned char> & binary);

    // Check if operand is valid byte and emit opcode and operand
    std::vector<unsigned char> generateByteOperandBinary(
        const std::string & byteExpr,
        mu::ParserInt & exprParser,
        std::map<std::string, unsigned char> & instrDefs
        );
};

}}
