#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>
#include <cassert>

#include "instruction.hpp"

namespace nimble65 { namespace lexer {

class InstrIndX : public Instruction
{
    public:
    InstrIndX(const int line, const int column, const int pos, std::string instrString,
        std::string addrExpr) :
        Instruction(line, column, pos, instrString, true),
        addrExpr(addrExpr)
        {};

    const std::string addrExpr;

    int size() override { return 2; }
    std::string sizeExpr() override { assert(false); return ""; }

    std::vector<unsigned char> InstrIndX::generateBinary(mu::ParserInt & exprParser) override {
        return generateByteOperandBinary(addrExpr, exprParser, instrDefs);
    }
        
    protected:
        std::map<std::string, unsigned char> instrDefs = {
            {"ora", 0x01},
            {"and", 0x21},
            {"eor", 0x41},
            {"adc", 0x61},
            {"sta", 0x81},
            {"lda", 0xa1},
            {"cmp", 0xc1},
            {"sbc", 0xe1},

            {"slo", 0x03},
            {"rla", 0x23},
            {"sre", 0x43},
            {"rra", 0x63},
            {"sax", 0x83},
            {"lax", 0xa3},
            {"dcp", 0xc3},
            {"isc", 0xe3},

            {"jam", 0x52}
        };
};

}}
