#pragma once

#include <string>
#include <memory>
#include <vector>

#include "instruction.hpp"
#include "instrimp.hpp"
#include "instrimm.hpp"
#include "constraint.hpp"
#include "muParserInt.h"


namespace nimble65 {

namespace lexer {

class Section
{
    public:
        const int line;
        const int column;
        const int number;
        const std::string label;
        bool hasAlign;
        const std::string alignExpr;
        bool hasOffset;
        const std::string offsetExpr;
        bool hasOrg;
        const std::string orgExpr;

        int size = 0;
        std::string unresolvedSize = "0";

        std::vector<std::unique_ptr<Instruction>> instructions;
        std::vector<Constraint> constraints;

        Section(const int line, const int column, std::string label);
        Section(const int line, const int column, std::string label, std::string alignExpr, bool hasOffset, std::string offsetExpr = "");
        Section(const int line, const int column, std::string label, std::string orgExpr);

        std::string getSizeExpr() { return std::to_string(size) + "+" + unresolvedSize; }

        // Gets and sets the start address, which is only valid once after linking
        void setStartAddress(int address) {
            assert(startAddress == -1);
            startAddress = address;
        }
        int getStartAddress() {
            assert(startAddress != -1);
            return startAddress;
        }

        // Returns the total size of this section in bytes. Assumes all
        // unresolvable operand expressions result in 2 bytes (i.e., addresses)
        // which should be true in the linking phase.
        // The actual calculation is done on first call; after that, the
        // total size remains constant.
        int getTotalSize(mu::ParserInt & exprParser);

        // Returns the requested value. Assumes expression can be resolved.
        // If not, a link_error is thrown.
        int getAlign(mu::ParserInt & exprParser);
        int getOffset(mu::ParserInt & exprParser);
        int getOrg(mu::ParserInt & exprParser);
        
        // Generate and return binary data out of this section.
        std::vector<unsigned char> generateBinary(mu::ParserInt & exprParser, unsigned char fillByte);

    private:
        static int nextNumber;

        // Resulting start address after linking
        int startAddress = -1;
        // Total size in bytes
        int totalSize = -1;

        // Resulting values after linking
        int align = -1;
        int offset = -1;
        int org = -1;

        // Tries to resolve an expression with the expression parser and throws a
        // link_error exception in case of failure
        int resolveExpression(mu::ParserInt & exprParser, std::string exprString, std::string errorMsg);
        
    };

}}
