#include "instrimp.hpp"
#include "exceptions.hpp"

#include "muParserInt.h"
#include "logger.hpp"

namespace nimble65 { namespace lexer {

std::vector<unsigned char> InstrImp::generateBinary(mu::ParserInt & exprParser) {
    std::vector<unsigned char> binary;
    assert(instrDefs.find(instrString) != instrDefs.end());
    binary.push_back(instrDefs[instrString]);
    return binary;
}

}}
