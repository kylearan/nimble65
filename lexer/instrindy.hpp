#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>
#include <cassert>

#include "instruction.hpp"

namespace nimble65 { namespace lexer {

class InstrIndY : public Instruction
{
    public:
    InstrIndY(const int line, const int column, const int pos, std::string instrString,
        std::string addrExpr) :
        Instruction(line, column, pos, instrString, true),
        addrExpr(addrExpr)
        {};

    const std::string addrExpr;

    int size() override { return 2; }
    std::string sizeExpr() override { assert(false); return ""; }

    std::vector<unsigned char> InstrIndY::generateBinary(mu::ParserInt & exprParser) override {
        return generateByteOperandBinary(addrExpr, exprParser, instrDefs);
    }
        
    protected:
        std::map<std::string, unsigned char> instrDefs = {
            {"ora", 0x11},
            {"and", 0x31},
            {"eor", 0x51},
            {"adc", 0x71},
            {"sta", 0x91},
            {"lda", 0xb1},
            {"cmp", 0xd1},
            {"sbc", 0xf1},

            {"slo", 0x13},
            {"rla", 0x33},
            {"sre", 0x53},
            {"rra", 0x73},
            {"lax", 0xb3},
            {"dcp", 0xd3},
            {"isc", 0xf3},

            {"sha", 0x93},

            {"jam", 0x62}
        };
};

}}
