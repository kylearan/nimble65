#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>
#include <cassert>

#include "instruction.hpp"

namespace nimble65 { namespace lexer {

class InstrRel : public Instruction
{
    public:
    InstrRel(const int line, const int colum, const int pos, std::string instrString,
        std::string addrExpr) :
        Instruction(line, column, pos, instrString, true),
        addrExpr(addrExpr)
        {};

    const std::string addrExpr;

    int size() override { return 2; }
    std::string sizeExpr() override { assert(false); return ""; }

    std::vector<unsigned char> generateBinary(mu::ParserInt & exprParser) override;
    
    protected:
        std::map<std::string, unsigned char> instrDefs = {
            {"bpl", 0x10},
            {"bmi", 0x30},
            {"bvc", 0x50},
            {"bvs", 0x70},
            {"bcc", 0x90},
            {"bcs", 0xb0},
            {"bne", 0xd0},
            {"beq", 0xf0}
        };
};

}}
