#pragma once

#include <string>
#include <sstream>

#include "token.hpp"

namespace nimble65 { namespace lexer
{
    //////////////////////////////////////////////////////////////////////
    // Symbol definition, either a concrete value or an expression.
    //////////////////////////////////////////////////////////////////////
    class Symbol : public Token {
    public:
        Symbol(int line, int column, std::string identifier, int value) :
            Token(line, column, TokenType::symbol),
            identifier(identifier),
            value(int(value)),
            expression("")
            {}
        Symbol(int line, int column, std::string identifier, std::string expression) :
            Token(line, column, TokenType::symbol),
            identifier(identifier),
            value(0),
            expression(expression)
            {}

        const TokenType type() { return TokenType::symbol; }

        std::string toString() const {
            std::ostringstream oss;
            oss << "Symbol at " << line << ", " << column << ": " << identifier << "=" << expression;
            return oss.str();
        }

        std::string identifier;
        double value;
        std::string expression;
    };
}}
