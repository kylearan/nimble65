#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>
#include <cassert>

#include "instruction.hpp"

namespace nimble65 { namespace lexer {

class InstrImm : public Instruction
{
    public:
    InstrImm(const int line, const int column, const int pos, std::string instrString,
        std::string exprString) :
        Instruction(line, column, pos, instrString, true),
        exprString(exprString)
        {};

    const std::string exprString;

    int size() override { return 2; }
    std::string sizeExpr() override { assert(false); return std::string("0"); }

    std::vector<unsigned char> InstrImm::generateBinary(mu::ParserInt & exprParser) override {
        return generateByteOperandBinary(exprString, exprParser, instrDefs);
    }
        
    protected:
        std::map<std::string, unsigned char> instrDefs = {
            {"lda", 0xa9},
            {"ldx", 0xa2},
            {"ldy", 0xa0},
            {"cmp", 0xc9},
            {"cpx", 0xe0},
            {"cpy", 0xc0},
            {"ora", 0x09},
            {"and", 0x29},
            {"eor", 0x49},
            {"adc", 0x69},
            {"sbc", 0xe9},

            {"anc", 0x0b},
            {"alr", 0x4b},
            {"arr", 0x6b},
            {"sbx", 0xcb},

            {"lax", 0xab},
            {"ane", 0x8b},
            {"nop", 0x80},
            {"jam", 0x12}
        };
};

}}
