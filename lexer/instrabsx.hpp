#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>
#include <cassert>

#include "instruction.hpp"
#include "instrabsbase.hpp"

namespace nimble65 { namespace lexer {

class InstrAbsX : public InstrAbsBase
{
    public:
    InstrAbsX(const int line, const int column, const int pos, std::string instrString,
        std::string addrExpr, bool forceWord) :
        InstrAbsBase(line, column, pos, instrString, addrExpr, forceWord)
        {};

        std::vector<unsigned char> InstrAbsX::generateBinary(mu::ParserInt & exprParser) override {
            return generateAbsBinary(exprParser, instrWordDefs, instrZpDefs, "absolute,x");
        }
                
        protected:
        std::map<std::string, unsigned char> instrWordDefs = {
            {"ldy", 0xbc},
            {"ora", 0x1d},
            {"and", 0x3d},
            {"eor", 0x5d},
            {"adc", 0x7d},
            {"sta", 0x9d},
            {"lda", 0xbd},
            {"cmp", 0xdd},
            {"sbc", 0xfd},
            {"asl", 0x1e},
            {"rol", 0x3e},
            {"lsr", 0x5e},
            {"ror", 0x7e},
            {"dec", 0xde},
            {"inc", 0xfe},

            {"slo", 0x1f},
            {"rla", 0x2f},
            {"sre", 0x5f},
            {"rra", 0x7f},
            {"dcp", 0xdf},
            {"isc", 0xff},

            {"shy", 0x9c},
            {"nop", 0x1c},
            {"jam", 0x92}
        };

        std::map<std::string, unsigned char> instrZpDefs = {
            {"ldy", 0xb4},
            {"ora", 0x15},
            {"and", 0x35},
            {"eor", 0x55},
            {"adc", 0x75},
            {"sta", 0x95},
            {"lda", 0xb5},
            {"cmp", 0xd5},
            {"sbc", 0xf5},
            {"asl", 0x16},
            {"rol", 0x36},
            {"lsr", 0x56},
            {"ror", 0x76},
            {"dec", 0xd6},
            {"inc", 0xf6},

            {"slo", 0x17},
            {"rla", 0x27},
            {"sre", 0x57},
            {"rra", 0x77},
            {"dcp", 0xd7},
            {"isc", 0xf7},

            {"nop", 0x14},
            {"jam", 0x32}
        };
};

}}
