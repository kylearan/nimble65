cmake_minimum_required(VERSION 3.0)

add_library(lexer STATIC
    ${CMAKE_CURRENT_SOURCE_DIR}/location.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/section.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/instruction.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/instrdata.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/instrimp.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/instrrel.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/instrind.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/instrabsbase.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/constraint.cpp
)
