
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include <pegtl.hh>
#include <pegtl/analyze.hh>
#include <pegtl/trace.hh>

#include "n65.hpp"
#include "nimble65.hpp"
#include "logger.hpp"
#include "exceptions.hpp"
#include "preprocessor/pp_common.hpp"
#include "preprocessor/pp_grammar.hpp"
#include "parser/assembler/common.hpp"
#include "parser/assembler/grammar.hpp"
#include "lexer/location.hpp"
#include "lexer/section.hpp"
#include "lexer/instrdata.hpp"
#include "lexer/instrimp.hpp"
#include "lexer/instrimm.hpp"
#include "lexer/instrabs.hpp"
#include "lexer/instrabsx.hpp"
#include "lexer/instrabsy.hpp"
#include "lexer/instrrel.hpp"
#include "lexer/instrind.hpp"
#include "lexer/instrindx.hpp"
#include "lexer/instrindy.hpp"

#include "parsingdata.hpp"

#include "muParserInt.h"
#include "muParserBase.h"
#include "muParserError.h"

#include "lexer/symbol.hpp"

#include "args.hxx"

extern LogConfig logCfg;

namespace nimble65 {
    
/* Preprocess source file. Return false on error.
 */
bool N65::preprocessSource(ParsingData & data) {
    try {
        LOG(LogLevel::INFO) << "Preprocessing file " << conf.inFilename;
        std::ifstream srcStream(conf.inFilename);
        std::stringstream srcBuffer;
        srcBuffer << srcStream.rdbuf();
        //std::string curSrc;
        data.preprocessedSource = srcBuffer.str();
        const int size = data.preprocessedSource.size();
        ParsingData::posInfo pi = {0, size, 0, conf.inFilename};
        data.posData.push_back(pi);
        // Do pre-processing until nothing more changes or max number of passes is reached
        do {
            data.curSrc = data.preprocessedSource;
            data.preprocessedSource.clear();
            data.numPass++;
            LOG(LogLevel::VERBOSE) << "Preprocessing pass " << data.numPass;
            pegtl::parse<nimble65::parser::preprocessor::pp_grammar, nimble65::parser::preprocessor::action, nimble65::parser::preprocessor::errors>(data.curSrc, conf.inFilename, data);            
        } while (data.preprocessedSource != data.curSrc && data.numPass < conf.maxPasses);
    } catch (std::runtime_error e) {
        LOG(LogLevel::ERR) << "Parse error: " << e.what();
        return false;
    } catch (...) {
        LOG(LogLevel::ERR) << "Parse error: Unknown exception!";
        return false;
    }
    return true;
}


/* Parse preprocessed source. Return false on error.
 */
bool N65::parseSource(ParsingData & data) {
    try {
        LOG(LogLevel::INFO) << "Parsing preprocessed source";
        pegtl::parse<nimble65::parser::assembler::grammar, nimble65::parser::assembler::action, nimble65::parser::assembler::errors>(data.preprocessedSource, conf.inFilename, data);
    } catch (std::runtime_error e) {
        LOG(LogLevel::ERR) << "Parse error: " << e.what();
        return false;
    } catch (...) {
        LOG(LogLevel::ERR) << "Parse error: Unknown exception!";
        return false;
    }
    data.printActiveTokens();
    return true;
}


/* Resolve symbols first time and link locations. Returns false on error.
 */
bool N65::resolveSymbolsAndLink(ParsingData & data) {
    try {
        LOG(LogLevel::VERBOSE) << "Resolving symbols pre-link";
        // Resolve as many symbols as possible
        data.resolveSymbols();
        if (logCfg.level == LogLevel::DEBUG) {
            data.printSymbols();
            data.printUnresolvedSymbols();    
        }

        // Link locations
        LOG(LogLevel::INFO) << "Linking locations";
        data.linkLocations();

        // Resolve rest of symbols
        LOG(LogLevel::VERBOSE) << "Resolving symbols post-link";
        data.resolveSymbols();
        if (logCfg.level == LogLevel::DEBUG) {
            data.printSymbols();
        }
        if (data.unresolvedSymbols.size() > 0) {
            LOG(LogLevel::ERR) << "Error: Unable to resolve all symbols!";
            data.printUnresolvedSymbols();
            return false;
        }
    } catch (nimble65::compile_error e) {
        LOG(LogLevel::ERR) << "Error at " + std::to_string(e.line) + ", " + std::to_string(e.column) + ": " << e.what();
        return false;
    } catch (nimble65::link_error e) {
        LOG(LogLevel::ERR) << "Error at " + std::to_string(e.line) + ", " + std::to_string(e.column) + ": " << e.what();
        return false;
    } catch (...) {
        LOG(LogLevel::ERR) << "Error: Unknown exception!";
        return false;
    }
    return true;
}


/* Generates the binary and writes it to disk. Returns false on error.
 */
bool N65::generateAndWriteBinary(ParsingData & data) {
    try {
        // Convert to binary
        LOG(LogLevel::INFO) << "Generating binary";
        int startAddress;
        auto binary = data.generateBinary(startAddress);
        LOG(LogLevel::INFO) << "Size: $" << std::hex << binary.size() << " ($" << startAddress << " to $" << startAddress + binary.size() - 1 << ")" << std::dec;

        // Write binary to file
        LOG(LogLevel::DEBUG) << "Writing generated binary to file " + conf.outFilename;
        std::ofstream outFile(conf.outFilename, std::ofstream::out | std::ofstream::binary);
        outFile.write((char *)binary.data(), binary.size());
    } catch (nimble65::compile_error e) {
        LOG(LogLevel::ERR) << "Error at " + std::to_string(e.line) + ", " + std::to_string(e.column) + ": " << e.what();
        return 1;
    } catch (nimble65::link_error e) {
        LOG(LogLevel::ERR) << "Error at " + std::to_string(e.line) + ", " + std::to_string(e.column) + ": " << e.what();
        return 1;
    } catch (...) {
        LOG(LogLevel::ERR) << "Error: Unknown exception!";
        return 1;
    }
   return true;
}

}
